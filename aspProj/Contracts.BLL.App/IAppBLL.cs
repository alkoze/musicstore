﻿using System;
using Contracts.BLL.App.Services;
using ee.itcollege.alkoze.musicstoreContracts.BLL.Base;

namespace Contracts.BLL.App
{
    public interface IAppBLL : IBaseBLL
    {
        ICartService Carts { get; }
        IItemInCartService ItemInCarts { get; }
        ICommentService Comments { get; }
        IDeliveryTypeService DeliveryTypes { get; }
        IDetailedCategoryService DetailedCategories { get; }
        IItemQualityService ItemQualities { get; }
        IItemService Items { get; }
        IItemStatusService ItemStatuses { get; }
        ILocationService Locations { get; }
        IMainCategoryService MainCategories { get; }
        IOrderService Orders { get; }
        IPaymentMethodService PaymentMethods { get; }
        IRatingService Ratings { get; }
        IPictureService Pictures { get; }
        ILangStrService LangStrs { get; }
        ILangStrTranslationService LangStrTranslation { get; }
    }
}