﻿ using ee.itcollege.alkoze.musicstoreContracts.BLL.Base.Mappers;
 using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace Contracts.BLL.App.Mappers
{
    public interface IItemServiceMapper: IBaseMapper<DALAppDTO.Item, BLLAppDTO.Item>
    {
        BLLAppDTO.ItemView MapItemView(DALAppDTO.ItemView inObject);
    }
}