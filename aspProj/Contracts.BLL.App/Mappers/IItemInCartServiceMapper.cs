﻿using ee.itcollege.alkoze.musicstoreContracts.BLL.Base.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace Contracts.BLL.App.Mappers
{
    public interface IItemInCartServiceMapper: IBaseMapper<DALAppDTO.ItemInCart, BLLAppDTO.ItemInCart>
    {
        
    }
}