﻿ using ee.itcollege.alkoze.musicstoreContracts.BLL.Base.Mappers;

 using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace Contracts.BLL.App.Mappers
{
    public interface IDetailedCategoryServiceMapper: IBaseMapper<DALAppDTO.DetailedCategory, BLLAppDTO.DetailedCategory>
    {
        BLLAppDTO.DetailedCategoryView MapDetailedCategoryView(DALAppDTO.DetailedCategoryView inObject);

    }
}