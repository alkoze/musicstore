﻿ using ee.itcollege.alkoze.musicstoreContracts.BLL.Base.Mappers;
 using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace Contracts.BLL.App.Mappers
{
    public interface IMainCategoryServiceMapper: IBaseMapper<DALAppDTO.MainCategory, BLLAppDTO.MainCategory>
    {
        BLLAppDTO.MainCategory MapMainCategory(DALAppDTO.MainCategory inObject);
    }
}