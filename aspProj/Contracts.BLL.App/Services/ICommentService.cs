﻿﻿using BLL.App.DTO;
using Contracts.DAL.App.Repositories;
 using ee.itcollege.alkoze.musicstoreContracts.BLL.Base.Services;

 namespace Contracts.BLL.App.Services
{
    public interface ICommentService : IBaseEntityService<Comment>, ICommentRepositoryCustom<CommentView>
    {
        
    }
}