﻿﻿using System;
 using System.Threading.Tasks;
 using BLL.App.DTO;
using Contracts.DAL.App.Repositories;
 using ee.itcollege.alkoze.musicstoreContracts.BLL.Base.Services;

 namespace Contracts.BLL.App.Services
{
    public interface ICartService: IBaseEntityService<Cart>, ICartRepositoryCustom<Cart>
    {
        Task UpdateItemCount(Guid? id);
    }
}