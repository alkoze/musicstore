﻿﻿using BLL.App.DTO;
using ee.itcollege.alkoze.musicstoreContracts.BLL.Base.Services;
using Contracts.DAL.App.Repositories;

namespace Contracts.BLL.App.Services
{
    public interface IOrderService: IBaseEntityService<Order>, IOrderRepositoryCustom<OrderView>
    {
        
    }
}