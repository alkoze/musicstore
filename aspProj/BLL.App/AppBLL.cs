﻿using System;
using BLL.App.Services;
using ee.itcollege.musicstore.BLL.Base;
using Contracts.BLL.App;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using DAL.App.EF;


namespace BLL.App
{
    public class AppBLL : BaseBLL<IAppUnitOfWork>, IAppBLL
    {
        public AppBLL(IAppUnitOfWork uow) : base(uow)
        {
        }

        public ICartService Carts =>
            GetService<ICartService>(() => new CartService(UOW));
        public IItemInCartService ItemInCarts =>
            GetService<IItemInCartService>(() => new ItemInCartService(UOW));

        public ICommentService Comments =>
            GetService<ICommentService>(() => new CommentService(UOW));

        public IDeliveryTypeService DeliveryTypes =>
            GetService<IDeliveryTypeService>(() => new DeliveryTypeService(UOW));
        public IDetailedCategoryService DetailedCategories =>
            GetService<IDetailedCategoryService>(() => new DetailedCategoryService(UOW));
        public IItemQualityService ItemQualities =>
            GetService<IItemQualityService>(() => new ItemQualityService(UOW));
        public IItemService Items =>
            GetService<IItemService>(() => new ItemService(UOW));
        public IItemStatusService ItemStatuses =>
            GetService<IItemStatusService>(() => new ItemStatusService(UOW));
        public ILocationService Locations =>
            GetService<ILocationService>(() => new LocationService(UOW));
        public IMainCategoryService MainCategories =>
            GetService<IMainCategoryService>(() => new MainCategoryService(UOW));
        public IOrderService Orders =>
            GetService<IOrderService>(() => new OrderService(UOW));
        public IPaymentMethodService PaymentMethods =>
            GetService<IPaymentMethodService>(() => new PaymentMethodService(UOW));
        public IRatingService Ratings =>
            GetService<IRatingService>(() => new RatingService(UOW));
        public IPictureService Pictures =>
            GetService<IPictureService>(() => new PictureService(UOW));
        public ILangStrService LangStrs =>
            GetService<ILangStrService>(() => new LangStrService(UOW));

        public ILangStrTranslationService LangStrTranslation =>
            GetService<ILangStrTranslationService>(() => new LangStrTranslationService(UOW));

    }
}