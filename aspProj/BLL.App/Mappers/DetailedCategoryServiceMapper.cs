﻿﻿using AutoMapper;
 using ee.itcollege.musicstore.BLL.Base.Mappers;
using Contracts.BLL.App.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
namespace BLL.App.Mappers
{
    public class DetailedCategoryServiceMapper: BaseMapper<DALAppDTO.DetailedCategory, BLLAppDTO.DetailedCategory>, IDetailedCategoryServiceMapper
    {
        public DetailedCategoryServiceMapper():base()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.DetailedCategoryView, BLLAppDTO.DetailedCategoryView>();
            MapperConfigurationExpression.CreateMap<DALAppDTO.Item, BLLAppDTO.Item>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public BLLAppDTO.DetailedCategoryView MapDetailedCategoryView(DALAppDTO.DetailedCategoryView inObject)
        {
            return Mapper.Map<BLLAppDTO.DetailedCategoryView>(inObject);
        }
    }
}