﻿﻿using AutoMapper;
 using ee.itcollege.musicstore.BLL.Base.Mappers;
 using Contracts.BLL.App.Mappers;
 using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;

namespace BLL.App.Mappers
{
    public class CartServiceMapper: BaseMapper<DALAppDTO.Cart, BLLAppDTO.Cart>, ICartServiceMapper
    {
        public CartServiceMapper():base()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.Cart, BLLAppDTO.Cart>();
            MapperConfigurationExpression.CreateMap<DALAppDTO.ItemView, BLLAppDTO.ItemView>();
            MapperConfigurationExpression.CreateMap<DALAppDTO.Picture, BLLAppDTO.Picture>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public BLLAppDTO.Cart MapCart(DALAppDTO.Cart inObject)
        {
            return Mapper.Map<BLLAppDTO.Cart>(inObject);
        }
    }
}