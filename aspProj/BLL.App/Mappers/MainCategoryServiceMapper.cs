﻿﻿using AutoMapper;
 using ee.itcollege.musicstore.BLL.Base.Mappers;
using Contracts.BLL.App.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
namespace BLL.App.Mappers
{
    public class MainCategoryServiceMapper: BaseMapper<DALAppDTO.MainCategory, BLLAppDTO.MainCategory>, IMainCategoryServiceMapper
    {
        public MainCategoryServiceMapper():base()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.MainCategory, BLLAppDTO.MainCategory>();
            MapperConfigurationExpression.CreateMap<DALAppDTO.DetailedCategory, BLLAppDTO.DetailedCategory>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public BLLAppDTO.MainCategory MapMainCategory(DALAppDTO.MainCategory inObject)
        {
            return Mapper.Map<BLLAppDTO.MainCategory>(inObject);
        }
    }
}