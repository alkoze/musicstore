﻿﻿using System.Collections.Generic;
 using AutoMapper;
 using ee.itcollege.musicstore.BLL.Base.Mappers;
using Contracts.BLL.App.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
namespace BLL.App.Mappers
{
    public class ItemServiceMapper: BaseMapper<DALAppDTO.Item, BLLAppDTO.Item>, IItemServiceMapper
    {

        public ItemServiceMapper():base()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.ItemView, BLLAppDTO.ItemView>();
            MapperConfigurationExpression.CreateMap<DALAppDTO.Picture, BLLAppDTO.Picture>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public BLLAppDTO.ItemView MapItemView(DALAppDTO.ItemView inObject)
        {
            return Mapper.Map<BLLAppDTO.ItemView>(inObject);
        }
    }
}