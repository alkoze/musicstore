﻿using Contracts.BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Mappers;
 using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
namespace BLL.App.Mappers
{
    public class DeliveryTypeServiceMapper: BaseMapper<DALAppDTO.DeliveryType, BLLAppDTO.DeliveryType>, IDeliveryTypeServiceMapper
    {
        
    }
}