﻿using AutoMapper;
using AutoMapper.Configuration;
using ee.itcollege.musicstore.BLL.Base.Mappers;

namespace BLL.App.Mappers
{
    public class BLLMapper<TLeftObject, TRightObject> : BaseMapper<TLeftObject, TRightObject>
        where TRightObject : class?, new()
        where TLeftObject : class?, new()
    {
        public BLLMapper() : base()
        { 
            // add more mappings
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Identity.AppUser, BLL.App.DTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Cart, BLL.App.DTO.Cart>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Comment, BLL.App.DTO.Comment>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.DeliveryType, BLL.App.DTO.DeliveryType>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.DetailedCategory, BLL.App.DTO.DetailedCategory>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Item, BLL.App.DTO.Item>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.ItemQuality, BLL.App.DTO.ItemQuality>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.ItemStatus, BLL.App.DTO.ItemQuality>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Location, BLL.App.DTO.Location>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.MainCategory, BLL.App.DTO.MainCategory>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Order, BLL.App.DTO.Order>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.PaymentMethod, BLL.App.DTO.PaymentMethod>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Picture, BLL.App.DTO.Picture>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Rating, BLL.App.DTO.Rating>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.ItemInCart, BLL.App.DTO.ItemInCart>();


            
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Rating, DAL.App.DTO.Rating>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Identity.AppUser, DAL.App.DTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Cart, DAL.App.DTO.Cart>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Comment, DAL.App.DTO.Comment>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.DeliveryType, DAL.App.DTO.DeliveryType>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.DetailedCategory, DAL.App.DTO.DetailedCategory>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Item, DAL.App.DTO.Item>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.ItemQuality, DAL.App.DTO.ItemQuality>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.ItemStatus, DAL.App.DTO.ItemStatus>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Location, DAL.App.DTO.Location>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.MainCategory, DAL.App.DTO.MainCategory>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Order, DAL.App.DTO.Order>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.PaymentMethod, DAL.App.DTO.PaymentMethod>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Picture, DAL.App.DTO.Picture>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.ItemInCart, DAL.App.DTO.ItemInCart>();


            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
    }
}