﻿﻿using AutoMapper;
 using ee.itcollege.musicstore.BLL.Base.Mappers;
using Contracts.BLL.App.Mappers;
using BLLAppDTO=BLL.App.DTO;
using DALAppDTO=DAL.App.DTO;
namespace BLL.App.Mappers
{
    public class CommentServiceMapper: BaseMapper<DALAppDTO.Comment, BLLAppDTO.Comment>, ICommentServiceMapper
    {
        public CommentServiceMapper():base()
        {
            MapperConfigurationExpression.CreateMap<DALAppDTO.CommentView, BLLAppDTO.CommentView>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
        public BLLAppDTO.CommentView MapCommentView(DALAppDTO.CommentView inObject)
        {
            return Mapper.Map<BLLAppDTO.CommentView>(inObject);
        }
    }
}