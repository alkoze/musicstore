﻿﻿using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using BLL.App.DTO;
 using BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class DetailedCategoryService:
        BaseEntityService<IAppUnitOfWork, IDetailedCategoryRepository, IDetailedCategoryServiceMapper,
            DAL.App.DTO.DetailedCategory, BLL.App.DTO.DetailedCategory>, IDetailedCategoryService
    {
        public DetailedCategoryService(IAppUnitOfWork uow) : base(uow, uow.DetailedCategories,
            new DetailedCategoryServiceMapper())
        {
        }
        public virtual async Task<IEnumerable<DetailedCategoryView>> GetAllForViewAsync()
        {
            return (await Repository.GetAllForViewAsync()).Select(e => Mapper.MapDetailedCategoryView(e));

        }
    }
}