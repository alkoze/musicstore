﻿﻿using BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class ItemQualityService:
        BaseEntityService<IAppUnitOfWork, IItemQualityRepository, IItemQualityServiceMapper,
            DAL.App.DTO.ItemQuality, BLL.App.DTO.ItemQuality>, IItemQualityService
    {
        public ItemQualityService(IAppUnitOfWork uow) : base(uow, uow.ItemQualities,
            new ItemQualityServiceMapper())
        {
        }
    }
}