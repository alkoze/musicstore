﻿﻿using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using BLL.App.DTO;
 using BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class CommentService:
        BaseEntityService<IAppUnitOfWork, ICommentRepository, ICommentServiceMapper,
            DAL.App.DTO.Comment, BLL.App.DTO.Comment>, ICommentService
    {
        public CommentService(IAppUnitOfWork uow) : base(uow, uow.Comments,
            new CommentServiceMapper())
        {
        }

        public virtual async Task<IEnumerable<CommentView>> GetAllForViewAsync()
        {
            return (await Repository.GetAllForViewAsync()).Select(e => Mapper.MapCommentView(e));

        }
    }
}