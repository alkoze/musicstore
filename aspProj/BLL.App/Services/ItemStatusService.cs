﻿﻿using BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class ItemStatusService:
        BaseEntityService<IAppUnitOfWork, IItemStatusRepository, IItemStatusServiceMapper,
            DAL.App.DTO.ItemStatus, BLL.App.DTO.ItemStatus>, IItemStatusService
    {
        public ItemStatusService(IAppUnitOfWork uow) : base(uow, uow.ItemStatuses,
            new ItemStatusServiceMapper())
        {
        }
    }
}