﻿﻿using System;
 using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using BLL.App.DTO;
 using BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Services;
 using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

 namespace BLL.App.Services
{
    public class CartService:
        BaseEntityService<IAppUnitOfWork, ICartRepository, ICartServiceMapper,
            DAL.App.DTO.Cart, BLL.App.DTO.Cart>, ICartService
    {
        public CartService(IAppUnitOfWork uow) : base(uow, uow.Carts,
            new CartServiceMapper())
        {
        }
        public virtual async Task<IEnumerable<Cart>> GetAllForViewAsync(Guid? id)
        {
            return (await Repository.GetAllForViewAsync(id)).Select(e => Mapper.MapCart(e));
        }

        public async Task UpdateItemCount(Guid? id)
        {
            var session = await UOW.Carts.GetAllForViewAsync(id);
            foreach (DAL.App.DTO.Cart cart in session)
            {
                cart.ItemCount = cart!.Items!.Count;
                await UOW.Carts.UpdateAsync(cart, id);
                await UOW.SaveChangesAsync();
            }
        }
    }
}