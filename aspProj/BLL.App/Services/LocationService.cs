﻿﻿using BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class LocationService:
        BaseEntityService<IAppUnitOfWork, ILocationRepository, ILocationServiceMapper,
            DAL.App.DTO.Location, BLL.App.DTO.Location>, ILocationService
    {
        public LocationService(IAppUnitOfWork uow) : base(uow, uow.Locations,
            new LocationServiceMapper())
        {
        }
    }
}