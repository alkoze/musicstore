﻿using BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class ItemInCartService:
        BaseEntityService<IAppUnitOfWork, IItemInCartRepository, IItemInCartServiceMapper,
            DAL.App.DTO.ItemInCart, BLL.App.DTO.ItemInCart>, IItemInCartService
    {
        public ItemInCartService(IAppUnitOfWork uow) : base(uow, uow.ItemInCarts,
            new ItemInCartServiceMapper())
        {
        }
    }
}