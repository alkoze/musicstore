﻿﻿using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using BLL.App.DTO;
 using BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class MainCategoryService:
        BaseEntityService<IAppUnitOfWork, IMainCategoryRepository, IMainCategoryServiceMapper,
            DAL.App.DTO.MainCategory, BLL.App.DTO.MainCategory>, IMainCategoryService
    {
        public MainCategoryService(IAppUnitOfWork uow) : base(uow, uow.MainCategories,
            new MainCategoryServiceMapper())
        {
        }
        public virtual async Task<IEnumerable<MainCategory>> GetAllForViewAsync()
        {
            return (await Repository.GetAllForViewAsync()).Select(e => Mapper.MapMainCategory(e));
        }
    }
}