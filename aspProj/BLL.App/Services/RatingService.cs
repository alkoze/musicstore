﻿﻿using BLL.App.Mappers;
 using ee.itcollege.musicstore.BLL.Base.Services;
using Contracts.BLL.App.Mappers;
using Contracts.BLL.App.Services;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;

namespace BLL.App.Services
{
    public class RatingService:
        BaseEntityService<IAppUnitOfWork, IRatingRepository, IRatingServiceMapper,
            DAL.App.DTO.Rating, BLL.App.DTO.Rating>, IRatingService
    {
        public RatingService(IAppUnitOfWork uow) : base(uow, uow.Ratings,
            new RatingServiceMapper())
        {
        }
    }
}