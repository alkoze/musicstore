﻿﻿using AutoMapper;

 namespace PublicApi.DTO.v1.Mappers
{
    public class CartMapper: BaseMapper<BLL.App.DTO.Cart, Cart>
    {
        public CartMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Cart, Cart>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.ItemView, ItemView>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Picture, Picture>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public Cart MapCart(BLL.App.DTO.Cart inObject)
        {
            return Mapper.Map<Cart>(inObject);
        }
    }
}