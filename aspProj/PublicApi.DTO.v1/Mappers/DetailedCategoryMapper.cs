﻿﻿using AutoMapper;

 namespace PublicApi.DTO.v1.Mappers
{
    public class DetailedCategoryMapper: BaseMapper<BLL.App.DTO.DetailedCategory, DetailedCategory>
    {
        public DetailedCategoryMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.DetailedCategoryView, DetailedCategoryView>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Item, Item>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public DetailedCategoryView MapDetailedCategoryView(BLL.App.DTO.DetailedCategoryView inObject)
        {
            return Mapper.Map<DetailedCategoryView>(inObject);
        }
    }
}