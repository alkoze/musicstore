﻿﻿using AutoMapper;

 namespace PublicApi.DTO.v1.Mappers
{
    public class CommentMapper: BaseMapper<BLL.App.DTO.Comment, Comment>
    {
        public CommentMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.CommentView, CommentView>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public CommentView MapCommentView(BLL.App.DTO.CommentView inObject)
        {
            return Mapper.Map<CommentView>(inObject);
        }
    }
}