﻿﻿using AutoMapper;

 namespace PublicApi.DTO.v1.Mappers
{
    public class ItemMapper: BaseMapper<BLL.App.DTO.Item, Item>
    {
        public ItemMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.ItemView, ItemView>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.Picture, Picture>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public ItemView MapItemView(BLL.App.DTO.ItemView inObject)
        {
            return Mapper.Map<ItemView>(inObject);
        }
    }
}