﻿﻿using AutoMapper;

 namespace PublicApi.DTO.v1.Mappers
{
    public class MainCategoryMapper: BaseMapper<BLL.App.DTO.MainCategory, MainCategory>
    {
        public MainCategoryMapper()
        {
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.MainCategory, MainCategory>();
            MapperConfigurationExpression.CreateMap<BLL.App.DTO.DetailedCategory, DetailedCategory>();
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }

        public MainCategory MapMainCategory(BLL.App.DTO.MainCategory inObject)
        {
            return Mapper.Map<MainCategory>(inObject);
        }
    }
}