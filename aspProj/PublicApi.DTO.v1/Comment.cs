﻿﻿using System;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;
using PublicApi.DTO.v1.Identity;

namespace PublicApi.DTO.v1
{
    public class Comment : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string CommentText { get; set; } = default!;
        
        public Guid? AppUserId { get; set; }
        public Guid? RatingId { get; set; }
        
        public Guid Creator { get; set; } = default!;

    }
}