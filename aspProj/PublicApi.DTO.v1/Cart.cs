﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class Cart : IDomainEntityId
    {
        public Guid Id { get; set; }
        public ICollection<ItemView>? Items { get; set; }
        public Guid AppUserId { get; set; }
        public int ItemCount { get; set; }

    }
}