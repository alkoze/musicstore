﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class ItemView
    {
        public Guid Id { get; set; }
        public string? ItemName { get; set; }
        public int? YearMade { get; set; }
        public string? ItemDescription { get; set; }
        public int? Price { get; set; }

        public string Location { get; set; } = default!;
        public string ItemQuality { get; set; } = default!;

        public string ItemStatus { get; set; } = default!;

        public string DetailedCategory { get; set; } = default!;
        public string MainCategory { get; set; } = default!;

        public string DisplayPictureUrl { get; set; } = default!;
        public Guid? LocationId { get; set; }
        public Guid? ItemQualityId { get; set; }

        public Guid? ItemStatusId { get; set; }

        public Guid? DetailedCategoryId { get; set; }
        public Guid? MainCategoryId { get; set; }
        
        public Guid? OrderId { get; set; }

        public Guid? AppUserId { get; set; }
        
        public ICollection<Picture>? Pictures { get; set; }

        public string SellerName { get; set; } = default!;
    }
}