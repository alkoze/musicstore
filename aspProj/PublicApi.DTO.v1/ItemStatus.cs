﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class ItemStatus : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string StatusName { get; set; } = default!;
        public ICollection<Item>? Items { get; set; }
    }
}