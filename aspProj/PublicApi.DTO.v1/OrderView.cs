﻿using System;
using System.Collections.Generic;

namespace PublicApi.DTO.v1
{
    public class OrderView
    {
        public Guid Id { get; set; }
        public Guid? DeliveryTypeId { get; set; }

        public string DeliveryTypeName { get; set; } = default!;
        public ICollection<Item>? Items { get; set; }
        public Guid AppUserId { get; set; }
        public int OrderPrice { get; set; }
        public string PaymentInfo { get; set; } = default!;

        public string DeliveryAddress { get; set; } = default!;

        public string PaymentMethodName { get; set; } = default!;

        public Guid PaymentMethodId { get; set; }

        public string PostalIndex { get; set; } = default!;
    }
}