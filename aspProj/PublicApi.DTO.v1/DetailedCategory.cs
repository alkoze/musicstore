﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class DetailedCategory : IDomainEntityId
    {
        public Guid Id { get; set; }
        
        public string DetailedCategoryName { get; set; } = default!;
        
        public Guid MainCategoryId { get; set; }
        public string? MainCategory { get; set; }
        
        public ICollection<Item>? Items { get; set; }
    }
}