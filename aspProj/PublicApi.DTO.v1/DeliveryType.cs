﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class DeliveryType : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string DeliveryTypeName { get; set; } = default!;
        public ICollection<Order>? Orders { get; set; }
        public int DeliveryTypePrice { get; set; } = default!;
    }
}