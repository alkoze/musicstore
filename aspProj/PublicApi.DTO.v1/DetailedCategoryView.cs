﻿using System;
using System.Collections.Generic;

namespace PublicApi.DTO.v1
{
    public class DetailedCategoryView
    {
        public Guid Id { get; set; }
        public string DetailedCategoryName { get; set; } = default!;
        
        public Guid MainCategoryId { get; set; }


        public string? MainCategory { get; set; }
        
        public ICollection<Item>? Items { get; set; }
    }
}