﻿using System;

namespace PublicApi.DTO.v1
{
    public class CommentView
    {
        public Guid Id { get; set; }
        public string CommentText { get; set; } = default!;
        
        public Guid? AppUserId { get; set; }
        public string Rating { get; set; } = default!;
        
        public Guid Creator { get; set; } = default!;

    }
}