﻿﻿using System;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;
using PublicApi.DTO.v1.Identity;

namespace PublicApi.DTO.v1
{
    public class PaymentMethod : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string PaymentMethodName { get; set; } = default!;
    }
}