﻿﻿using System;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace PublicApi.DTO.v1
{
    public class Picture : IDomainEntityId
    {
        public Guid Id { get; set; }
        public Guid ItemId { get; set; }
        public string PictureUrl { get; set; } = default!;

    }
}