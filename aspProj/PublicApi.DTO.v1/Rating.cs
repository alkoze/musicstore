﻿﻿using System;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;
using PublicApi.DTO.v1.Identity;

namespace PublicApi.DTO.v1
{
    public class Rating : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string RatingValue { get; set; } = default!;
    }
}