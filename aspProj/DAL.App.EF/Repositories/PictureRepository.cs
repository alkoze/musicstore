﻿﻿using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

using Domain;

namespace DAL.App.EF.Repositories
{
    public class PictureRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Picture, DAL.App.DTO.Picture>,
        IPictureRepository
    {
        public PictureRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Picture, DTO.Picture>())
        {
            
        }
    }
}