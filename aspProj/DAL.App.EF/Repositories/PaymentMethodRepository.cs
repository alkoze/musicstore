﻿﻿using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

using Domain;

namespace DAL.App.EF.Repositories
{
    public class PaymentMethodRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.PaymentMethod, DAL.App.DTO.PaymentMethod>,
        IPaymentMethodRepository
    {
        public PaymentMethodRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.PaymentMethod, DTO.PaymentMethod>())
        {
            
        }
    }
}