﻿﻿using System;
using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
 using DAL.App.DTO;
 using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

using Domain;
 using Microsoft.EntityFrameworkCore;

 namespace DAL.App.EF.Repositories
{
    public class DetailedCategoryRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.DetailedCategory, DAL.App.DTO.DetailedCategory>,
        IDetailedCategoryRepository
    {
        public DetailedCategoryRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.DetailedCategory, DTO.DetailedCategory>())
        {
            
        }


        public virtual async Task<IEnumerable<DetailedCategoryView>> GetAllForViewAsync()
        {
            var query = RepoDbSet;
            var result = await query
                .Select(a => new DetailedCategoryView()
                {
                    Id = a.Id,
                    DetailedCategoryName = a.DetailedCategoryName,
                    MainCategory = a.MainCategory!.MainCategoryName,
                    MainCategoryId = a.MainCategoryId,
                    Items = a.Items.Select(items => new Item
                    {
                        Id = items.Id,
                        ItemName = items.ItemName
                    }).ToList()
                }).ToListAsync();
            return result;
        }
    }
}