﻿﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

using Domain;

namespace DAL.App.EF.Repositories
{
    public class ItemQualityRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.ItemQuality, DAL.App.DTO.ItemQuality>,
        IItemQualityRepository
    {
        public ItemQualityRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.ItemQuality, DTO.ItemQuality>())
        {
            
        }
        
    }
}