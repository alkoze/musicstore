﻿﻿using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

using Domain;

namespace DAL.App.EF.Repositories
{
    public class LocationRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Location, DAL.App.DTO.Location>,
        ILocationRepository
    {
        public LocationRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Location, DTO.Location>())
        {
            
        }
    }
}