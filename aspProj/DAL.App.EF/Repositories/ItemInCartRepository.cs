﻿using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

namespace DAL.App.EF.Repositories
{
    public class ItemInCartRepository: EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.ItemInCart, DAL.App.DTO.ItemInCart>,
        IItemInCartRepository
    {
        public ItemInCartRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.ItemInCart, DTO.ItemInCart>())
        {
            
        }
    }
}