﻿﻿using System;
using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using AutoMapper;
 using AutoMapper.Configuration;
 using AutoMapper.QueryableExtensions;
 using Contracts.DAL.App.Repositories;
 using DAL.App.DTO;
 using DAL.App.DTO.Identity;
 using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

using Domain;
 using Microsoft.EntityFrameworkCore;
 using Item = Domain.App.Item;

 namespace DAL.App.EF.Repositories
{
    public class ItemRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Item, DAL.App.DTO.Item>,
        IItemRepository
    {
        public ItemRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Item, DTO.Item>())
        {
        }

        public virtual async Task<IEnumerable<ItemView>> GetAllForViewAsync(Guid? id)
        {
            List<ItemView> result;
            IQueryable<Item> query = RepoDbSet;
            if (id != null)
            {
                query = query.Where(item => item.Id == id);
            }
            result = await query
                .Select(a => new ItemView()
                {
                    Id = a.Id,
                    ItemName = a.ItemName,
                    ItemDescription = a.ItemDescription,
                    YearMade = a.YearMade,
                    Price = a.Price,
                    Location = a.Location!.LocationName,
                    ItemQuality = a.ItemQuality!.QualityName,
                    ItemStatus = a.ItemStatus!.StatusName,
                    DetailedCategory = a.DetailedCategory!.DetailedCategoryName,
                    MainCategory = a.MainCategory!.MainCategoryName,
                    DisplayPictureUrl = a.Pictures.FirstOrDefault().PictureUrl,
                    LocationId = a.LocationId,
                    ItemQualityId = a.ItemQualityId,
                    ItemStatusId = a.ItemStatusId,
                    MainCategoryId = a.MainCategoryId,
                    DetailedCategoryId = a.DetailedCategoryId,
                    AppUserId = a.AppUserId,
                    OrderId = a.OrderId,
                    SellerName = a.AppUser.Email,
                    Pictures = a.Pictures.Select(picture => new Picture
                    {
                        PictureUrl = picture.PictureUrl
                    }).ToList()
                }).ToListAsync();
            return result;
        }
    }
}