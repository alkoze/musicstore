﻿﻿using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using Contracts.DAL.App.Repositories;
 using DAL.App.DTO;
 using DAL.App.EF.Mappers;
using Domain;
 using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;
 using Microsoft.EntityFrameworkCore;

 namespace DAL.App.EF.Repositories
{
    public class CommentRepository: EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Comment, DAL.App.DTO.Comment>,
        ICommentRepository
    {
        public CommentRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Comment, DTO.Comment>())
        {
            
        }

        public virtual async Task<IEnumerable<CommentView>> GetAllForViewAsync()
        {
            var query = RepoDbSet;
            
            var result = await query
                .Select(a => new CommentView()
                {
                    Id = a.Id,
                    CommentText = a.CommentText,
                    AppUserId = a.AppUserId,
                    Rating = a.Rating!.RatingValue,
                    RatingId = a.RatingId,
                    Creator = a.Creator

                }).ToListAsync();
            return result;
        }
    }
}