﻿﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;
using Domain;

 namespace DAL.App.EF.Repositories
{
    public class DeliveryTypeRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.DeliveryType, DAL.App.DTO.DeliveryType>,
        IDeliveryTypeRepository
    {
        public DeliveryTypeRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.DeliveryType, DTO.DeliveryType>())
        {
            
        }

    }
}