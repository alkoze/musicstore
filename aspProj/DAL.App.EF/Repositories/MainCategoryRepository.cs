﻿﻿using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

using Domain;
 using Domain.App;
 using Microsoft.EntityFrameworkCore;
 using Microsoft.EntityFrameworkCore.Internal;
 using DetailedCategory = DAL.App.DTO.DetailedCategory;

 namespace DAL.App.EF.Repositories
{
    public class MainCategoryRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser,Domain.App.MainCategory, DAL.App.DTO.MainCategory>,
        IMainCategoryRepository
    {
        public MainCategoryRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.MainCategory, DTO.MainCategory>())
        {
            
        }
        
        public virtual async Task<IEnumerable<DTO.MainCategory>> GetAllForViewAsync()
        { 
            IEnumerable<DAL.App.DTO.MainCategory> result;
            IQueryable<MainCategory> query = RepoDbSet;
            result = await query
                .Select(g => new DAL.App.DTO.MainCategory()
                {
                    Id = g.Id,
                    MainCategoryName = g.MainCategoryName,
                    DetailedCategories = g.DetailedCategories.Select(a => new DetailedCategory
                    {
                        Id = a.Id,
                        DetailedCategoryName = a.DetailedCategoryName
                    }) as ICollection<DetailedCategory>
                }).ToListAsync();
            return result;
        }
    }
}