﻿﻿using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

using Domain;

namespace DAL.App.EF.Repositories
{
    public class RatingRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Rating, DAL.App.DTO.Rating>,
        IRatingRepository
    {
        public RatingRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Rating, DTO.Rating>())
        {
            
        }
    }
}