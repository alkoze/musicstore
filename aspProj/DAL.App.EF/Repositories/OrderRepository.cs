﻿﻿using System.Collections.Generic;
 using System.Linq;
 using System.Threading.Tasks;
 using Contracts.DAL.App.Repositories;
 using DAL.App.DTO;
 using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;
using Domain;
 using Microsoft.EntityFrameworkCore;

 namespace DAL.App.EF.Repositories
{
    public class OrderRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Order, DAL.App.DTO.Order>,
        IOrderRepository
    {
        public OrderRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Order, DTO.Order>())
        {
            
        }

        public virtual async Task<IEnumerable<OrderView>> GetAllForViewAsync()
        {
            var query = RepoDbSet;
            var result = await query
                .Select(a => new OrderView()
                {
                    Id = a.Id,
                    DeliveryTypeName = a.DeliveryType!.DeliveryTypeName,
                    DeliveryTypeId = a.DeliveryTypeId,
                    AppUserId = a.AppUserId,
                    Items = a.Items.Select(items => new Item
                    {
                        Id = items.Id,
                        ItemName = items.ItemName,
                        Price = items.Price,
                        OrderId = items.OrderId
                    }).ToList(),
                    OrderPrice = a.OrderPrice,
                    PaymentInfo = a.PaymentInfo,
                    DeliveryAddress = a.DeliveryAddress,
                    PostalIndex = a.PostalIndex,
                    PaymentMethodId = a.PaymentMethodId,
                    PaymentMethodName = a.PaymentMethod!.PaymentMethodName
                }).ToListAsync();
            return result;
        }
    }
}