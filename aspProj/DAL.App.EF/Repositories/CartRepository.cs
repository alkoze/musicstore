﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.DAL.App.Repositories;
using DAL.App.DTO;
using DAL.App.EF.Mappers;
using Domain;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;
using Microsoft.EntityFrameworkCore;
using Cart = Domain.App.Cart;

 namespace DAL.App.EF.Repositories
{
    public class CartRepository: EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.Cart, DAL.App.DTO.Cart>,
        ICartRepository
    {
        public CartRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.Cart, DTO.Cart>())
        {
            
        }
        
        public virtual async Task<IEnumerable<DTO.Cart>> GetAllForViewAsync(Guid? id)
        { 
            IEnumerable<DAL.App.DTO.Cart> result;
            IQueryable<Cart> query = RepoDbSet;
            if (id != null)
            {
                query = query.Where(item => item.Id == id);
            }
            result = await query
                .Select(g => new DAL.App.DTO.Cart()
                {
                    Id = g.Id,
                    ItemCount = g.ItemCount,
                    AppUserId = g.AppUserId,
                    Items = g.ItemInCarts.Select(x => x.Item).Select(a => new ItemView()
                    {
                        Id = a!.Id,
                        ItemName = a.ItemName,
                        ItemDescription = a.ItemDescription,
                        YearMade = a.YearMade,
                        Price = a.Price,
                        Location = a.Location!.LocationName,
                        ItemQuality = a.ItemQuality!.QualityName,
                        ItemStatus = a.ItemStatus!.StatusName,
                        DetailedCategory = a.DetailedCategory!.DetailedCategoryName,
                        MainCategory = a.MainCategory!.MainCategoryName,
                        DisplayPictureUrl = a.Pictures.FirstOrDefault().PictureUrl,
                        LocationId = a.LocationId,
                        AppUserId = a.AppUserId,
                        OrderId = a.OrderId,
                        ItemQualityId = a.ItemQualityId,
                        ItemStatusId = a.ItemStatusId,
                        MainCategoryId = a.MainCategoryId,
                        DetailedCategoryId = a.DetailedCategoryId,
                        Pictures = a.Pictures.Select(picture => new Picture
                        {
                            PictureUrl = picture.PictureUrl
                        }).ToList()
                    }).ToList()
                }).ToListAsync();
            return result;
        }
    }
}