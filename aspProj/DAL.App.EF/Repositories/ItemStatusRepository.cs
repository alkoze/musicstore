﻿﻿using Contracts.DAL.App.Repositories;
using DAL.App.EF.Mappers;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF.Repositories;

using Domain;

namespace DAL.App.EF.Repositories
{
    public class ItemStatusRepository : EFBaseRepository<AppDbContext, Domain.App.Identity.AppUser, Domain.App.ItemStatus, DAL.App.DTO.ItemStatus>,
        IItemStatusRepository
    {
        public ItemStatusRepository(AppDbContext repoDbContext) : base(repoDbContext,
            new DALMapper<Domain.App.ItemStatus, DTO.ItemStatus>())
        {
            
        }
    }
}