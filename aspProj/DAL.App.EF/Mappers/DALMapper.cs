using AutoMapper;
using ee.itcollege.alkoze.musicstore.DAL.Base.Mappers;

namespace DAL.App.EF.Mappers
{
    public class DALMapper<TLeftObject, TRightObject> : BaseMapper<TLeftObject, TRightObject>
        where TRightObject : class?, new()
        where TLeftObject : class?, new()
    {
        public DALMapper() : base()
        { 
            // add more mappings
            
            MapperConfigurationExpression.CreateMap<Domain.App.Identity.AppUser, DAL.App.DTO.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<Domain.App.Cart, DAL.App.DTO.Cart>();
            MapperConfigurationExpression.CreateMap<Domain.App.Comment, DAL.App.DTO.Comment>();
            MapperConfigurationExpression.CreateMap<Domain.App.DeliveryType, DAL.App.DTO.DeliveryType>();
            MapperConfigurationExpression.CreateMap<Domain.App.DetailedCategory, DAL.App.DTO.DetailedCategory>();
            MapperConfigurationExpression.CreateMap<Domain.App.Item, DAL.App.DTO.Item>();
            MapperConfigurationExpression.CreateMap<Domain.App.ItemQuality, DAL.App.DTO.ItemQuality>();
            MapperConfigurationExpression.CreateMap<Domain.App.ItemStatus, DAL.App.DTO.ItemQuality>();
            MapperConfigurationExpression.CreateMap<Domain.App.Location, DAL.App.DTO.Location>();
            MapperConfigurationExpression.CreateMap<Domain.App.MainCategory, DAL.App.DTO.MainCategory>();
            MapperConfigurationExpression.CreateMap<Domain.App.Order, DAL.App.DTO.Order>();
            MapperConfigurationExpression.CreateMap<Domain.App.PaymentMethod, DAL.App.DTO.PaymentMethod>();
            MapperConfigurationExpression.CreateMap<Domain.App.Picture, DAL.App.DTO.Picture>();
            MapperConfigurationExpression.CreateMap<Domain.App.Rating, DAL.App.DTO.Rating>();
            MapperConfigurationExpression.CreateMap<Domain.App.ItemInCart, DAL.App.DTO.ItemInCart>();


            
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Rating, Domain.App.Rating>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Identity.AppUser, Domain.App.Identity.AppUser>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Cart, Domain.App.Cart>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Comment, Domain.App.Comment>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.DeliveryType, Domain.App.DeliveryType>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.DetailedCategory, Domain.App.DetailedCategory>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Item, Domain.App.Item>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.ItemQuality, Domain.App.ItemQuality>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.ItemStatus, Domain.App.ItemStatus>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Location, Domain.App.Location>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.MainCategory, Domain.App.MainCategory>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Order, Domain.App.Order>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.PaymentMethod, Domain.App.PaymentMethod>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.Picture, Domain.App.Picture>();
            MapperConfigurationExpression.CreateMap<DAL.App.DTO.ItemInCart, Domain.App.ItemInCart>();



            
            Mapper = new Mapper(new MapperConfiguration(MapperConfigurationExpression));
        }
    }
}