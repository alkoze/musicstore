using System;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using DAL.App.EF.Repositories;
using ee.itcollege.alkoze.musicstore.DAL.Base.EF;

namespace DAL.App.EF
{
    public class AppUnitOfOfWork : EFBaseUnitOfWork<Guid, AppDbContext>, IAppUnitOfWork
    {
        public AppUnitOfOfWork(AppDbContext uowDbContext) : base(uowDbContext)
        {
        }

        public ICartRepository Carts  => 
            GetRepository<ICartRepository>(() => new CartRepository(UOWDbContext));
        
        public IItemInCartRepository ItemInCarts  => 
            GetRepository<IItemInCartRepository>(() => new ItemInCartRepository(UOWDbContext));

        public ICommentRepository Comments => 
            GetRepository<ICommentRepository>(() => new CommentRepository(UOWDbContext));
        public IDeliveryTypeRepository DeliveryTypes  => 
            GetRepository<IDeliveryTypeRepository>(() => new DeliveryTypeRepository(UOWDbContext));
        public IDetailedCategoryRepository DetailedCategories  => 
            GetRepository<IDetailedCategoryRepository>(() => new DetailedCategoryRepository(UOWDbContext));
        public IItemQualityRepository ItemQualities  => 
            GetRepository<IItemQualityRepository>(() => new ItemQualityRepository(UOWDbContext));
        public IItemRepository Items  =>
            GetRepository<IItemRepository>(() => new ItemRepository(UOWDbContext));
        public IItemStatusRepository ItemStatuses  => 
            GetRepository<IItemStatusRepository>(() => new ItemStatusRepository(UOWDbContext));
        public ILocationRepository Locations  => 
            GetRepository<ILocationRepository>(() => new LocationRepository(UOWDbContext));
        public IMainCategoryRepository MainCategories => 
            GetRepository<IMainCategoryRepository>(() => new MainCategoryRepository(UOWDbContext));
        public IOrderRepository Orders  => 
            GetRepository<IOrderRepository>(() => new OrderRepository(UOWDbContext));
        public IPaymentMethodRepository PaymentMethods  => 
            GetRepository<IPaymentMethodRepository>(() => new PaymentMethodRepository(UOWDbContext));
        public IPictureRepository Pictures  => 
            GetRepository<IPictureRepository>(() => new PictureRepository(UOWDbContext));
        public IRatingRepository Ratings  => 
            GetRepository<IRatingRepository>(() => new RatingRepository(UOWDbContext));
        public ILangStrRepository LangStrs =>
            GetRepository<ILangStrRepository>(() => new LangStrRepository(UOWDbContext));

        public ILangStrTranslationRepository LangStrTranslations =>
            GetRepository<ILangStrTranslationRepository>(() => new LangStrTranslationRepository(UOWDbContext));

    }
}