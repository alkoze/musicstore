﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;
using Domain.App;
using Domain.App.Identity;
using Domain.Base;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace DAL.App.EF
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class AppDbContext : IdentityDbContext<AppUser, AppRole, Guid>, IBaseEntityTracker
    {
        private readonly IUserNameProvider _userNameProvider;

        public DbSet<Cart> Carts { get; set; } = default!;
        public DbSet<Comment> Comments { get; set; } = default!;
        public DbSet<DeliveryType> DeliveryTypes { get; set; } = default!;
        public DbSet<DetailedCategory> DetailedCategories { get; set; } = default!;
        public DbSet<Item> Items { get; set; } = default!;
        public DbSet<ItemQuality> ItemQualities { get; set; } = default!;
        public DbSet<ItemStatus> ItemStatuses { get; set; } = default!;
        public DbSet<Location> Locations { get; set; } = default!;
        public DbSet<MainCategory> MainCategories { get; set; } = default!;
        public DbSet<Order> Orders { get; set; } = default!;
        public DbSet<PaymentMethod> PaymentMethods { get; set; } = default!;
        public DbSet<Picture> Pictures { get; set; } = default!;
        public DbSet<Rating> Ratings { get; set; } = default!;
        
        public DbSet<ItemInCart> ItemInCarts { get; set; } = default!;



        public DbSet<LangStr> LangStrs { get; set; } = default!;
        public DbSet<LangStrTranslation> LangStrTranslation { get; set; } = default!;

        private readonly Dictionary<IDomainEntityId<Guid>, IDomainEntityId<Guid>> _entityTracker =
            new Dictionary<IDomainEntityId<Guid>, IDomainEntityId<Guid>>();

        public AppDbContext(DbContextOptions<AppDbContext> options, IUserNameProvider userNameProvider) : base(options)
        {
            _userNameProvider = userNameProvider;
        }

        public void AddToEntityTracker(IDomainEntityId<Guid> internalEntity, IDomainEntityId<Guid> externalEntity)
        {
            _entityTracker.Add(internalEntity, externalEntity);
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
            // enable cascade delete on GpsSession->GpsLocation
            /*
            builder.Entity<GpsSession>()
                .HasMany(s => s.GpsLocations)
                .WithOne(l => l.GpsSession!)
                .OnDelete(DeleteBehavior.Cascade);

            // enable cascade delete on LangStr->LangStrTranslations
            builder.Entity<LangStr>()
                .HasMany(s => s.Translations)
                .WithOne(l => l.LangStr!)
                .OnDelete(DeleteBehavior.Cascade);

            // indexes
            builder.Entity<GpsSession>().HasIndex(i => i.CreatedAt);
            builder.Entity<GpsSession>().HasIndex(i => i.RecordedAt);
            builder.Entity<GpsLocation>().HasIndex(i => i.CreatedAt);
            builder.Entity<GpsLocation>().HasIndex(i => i.RecordedAt);
            */
            builder.Entity<LangStrTranslation>().HasIndex(i => new {i.Culture, i.LangStrId}).IsUnique();
            foreach (var relationship in builder.Model
                .GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }
            
            builder.Entity<LangStr>()
                .HasMany(s => s.Translations)
                .WithOne(l => l.LangStr!)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Item>()
                .HasMany(s => s.Pictures)
                .WithOne(l => l.Item!)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<MainCategory>()
                .HasMany(s => s.DetailedCategories)
                .WithOne(l => l.MainCategory!)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<DetailedCategory>()
                .HasMany(s => s.Items)
                .WithOne(l => l.DetailedCategory!)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Cart>()
                .HasMany(s => s.ItemInCarts)
                .WithOne(l => l.Cart!)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Item>()
                .HasMany(s => s.ItemInCarts)
                .WithOne(l => l.Item!)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ItemQuality>()
                .HasMany(s => s.Items)
                .WithOne(l => l.ItemQuality!)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<ItemStatus>()
                .HasMany(s => s.Items)
                .WithOne(l => l.ItemStatus!)
                .OnDelete(DeleteBehavior.Cascade);
            builder.Entity<Location>()
                .HasMany(s => s.Items)
                .WithOne(l => l.Location!)
                .OnDelete(DeleteBehavior.Cascade);

        }

        private void SaveChangesMetadataUpdate()
        {
            // update the state of ef tracked objects
            ChangeTracker.DetectChanges();

            var markedAsAdded = ChangeTracker.Entries().Where(x => x.State == EntityState.Added);
            foreach (var entityEntry in markedAsAdded)
            {
                if (entityEntry.Entity is DomainEntityIdMetadataUser<AppUser> entityWithMetaDataUser)
                {
                    
                    entityWithMetaDataUser.CreatedAt = DateTime.Now;
                    entityWithMetaDataUser.CreatedBy = _userNameProvider.CurrentUserName;
                    entityWithMetaDataUser.ChangedAt = entityWithMetaDataUser.CreatedAt;
                    entityWithMetaDataUser.ChangedBy = entityWithMetaDataUser.CreatedBy;  
                }else if (entityEntry.Entity is DomainEntityIdMetadata entityWithMetaData)
                {
                    entityWithMetaData.CreatedAt = DateTime.Now;
                    entityWithMetaData.CreatedBy = _userNameProvider.CurrentUserName;
                    entityWithMetaData.ChangedAt = entityWithMetaData.CreatedAt;
                    entityWithMetaData.ChangedBy = entityWithMetaData.CreatedBy;  
                }
            }
            var markedAsModified = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified);
            foreach (var entityEntry in markedAsModified)
            {
                // check for IDomainEntityMetadata
                if (entityEntry.Entity is DomainEntityIdMetadataUser<AppUser> entityWithMetaDataUser)
                {
                    entityWithMetaDataUser.ChangedAt = DateTime.Now;
                    entityWithMetaDataUser.ChangedBy = _userNameProvider.CurrentUserName;

                    // do not let changes on these properties get into generated db sentences - db keeps old values
                    entityEntry.Property(nameof(entityWithMetaDataUser.CreatedAt)).IsModified = false;
                    entityEntry.Property(nameof(entityWithMetaDataUser.CreatedBy)).IsModified = false;
                }
                else if (entityEntry.Entity is DomainEntityIdMetadata entityWithMetaData)
                {
                    entityWithMetaData.ChangedAt = DateTime.Now;
                    entityWithMetaData.ChangedBy = _userNameProvider.CurrentUserName;

                    // do not let changes on these properties get into generated db sentences - db keeps old values
                    entityEntry.Property(nameof(entityWithMetaData.CreatedAt)).IsModified = false;
                    entityEntry.Property(nameof(entityWithMetaData.CreatedBy)).IsModified = false;
                }
            }
        }

        private void UpdateTrackedEntities()
        {
            foreach (var (key, value) in _entityTracker)
            {
                value.Id = key.Id;
            }
        }

        public override int SaveChanges()
        {
            SaveChangesMetadataUpdate();
            var result = base.SaveChanges();
            UpdateTrackedEntities();
            return result;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            SaveChangesMetadataUpdate();
            var result = base.SaveChangesAsync(cancellationToken);
            UpdateTrackedEntities();
            return result;
        }
    }
}