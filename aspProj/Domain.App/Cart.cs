﻿﻿using System;
 using System.Collections.Generic;
 using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class Cart : DomainEntityIdMetadataUser<AppUser>
    {
        public ICollection<ItemInCart>? ItemInCarts { get; set; }
        public int ItemCount { get; set; }
    }
}