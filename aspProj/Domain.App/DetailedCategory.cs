﻿﻿using System;
using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using Domain.Base;

namespace Domain.App
{
    public class DetailedCategory : DomainEntityIdMetadata
    {
        [Display(Name = nameof(DetailedCategoryName),
            ResourceType = typeof(Resources.Views.DetailedCategory.DetailedCategory))]
        public string DetailedCategoryName { get; set; } = default!;
        [Display(Name = nameof(MainCategoryId),
            ResourceType = typeof(Resources.Views.DetailedCategory.DetailedCategory))]
        public Guid MainCategoryId { get; set; } = default!;
        [Display(Name = nameof(MainCategory),
            ResourceType = typeof(Resources.Views.DetailedCategory.DetailedCategory))]
        public MainCategory? MainCategory { get; set; }
        
        public ICollection<Item>? Items { get; set; }
        
    }
}