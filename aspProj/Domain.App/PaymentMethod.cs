﻿﻿using System;
 using System.ComponentModel.DataAnnotations;
 using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class PaymentMethod : DomainEntityIdMetadata
    {
        [Display(Name = nameof(PaymentMethodName),
            ResourceType = typeof(Resources.Views.PaymentMethod.PaymentMethod))]
        public string PaymentMethodName { get; set; } = default!;
    }
}