﻿﻿using System;
 using System.ComponentModel.DataAnnotations;
 using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class Rating : DomainEntityIdMetadata
    {
        [Display(Name = nameof(RatingValue),
            ResourceType = typeof(Resources.Views.Rating.Rating))]
        public String RatingValue { get; set; } = default!;
    }
    
}