﻿﻿using System;
using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class Order : DomainEntityIdMetadataUser<AppUser>
    {
        [Display(Name = nameof(DeliveryType),
            ResourceType = typeof(Resources.Views.Order.Order))]
        public DeliveryType? DeliveryType { get; set; }
        [Display(Name = nameof(DeliveryTypeId),
            ResourceType = typeof(Resources.Views.Order.Order))]
        public Guid? DeliveryTypeId { get; set; }
        public ICollection<Item>? Items { get; set; }

        [Display(Name = nameof(PaymentInfo),
            ResourceType = typeof(Resources.Views.Order.Order))]
        [MinLength(1)]
        [MaxLength(256)]
        public string PaymentInfo { get; set; } = default!;

        [Display(Name = nameof(DeliveryAddress),
            ResourceType = typeof(Resources.Views.Order.Order))]
        [MinLength(1)]
        [MaxLength(256)]
        public string DeliveryAddress { get; set; } = default!;

        [Display(Name = nameof(PaymentMethod),
            ResourceType = typeof(Resources.Views.Order.Order))]
        public PaymentMethod PaymentMethod { get; set; } = default!;

        [Display(Name = nameof(PaymentMethodId),
            ResourceType = typeof(Resources.Views.Order.Order))]
        public Guid PaymentMethodId { get; set; } = default!;

        [Display(Name = nameof(PostalIndex),
            ResourceType = typeof(Resources.Views.Order.Order))]
        public string PostalIndex { get; set; } = default!;
        [Display(Name = nameof(OrderPrice),
            ResourceType = typeof(Resources.Views.Order.Order))]
        public int OrderPrice { get; set; } = default!;
    }
}