﻿﻿using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using Domain.Base;

namespace Domain.App
{
    public class MainCategory : DomainEntityIdMetadata
    {
        [Display(Name = nameof(MainCategoryName),
            ResourceType = typeof(Resources.Views.MainCategory.MainCategory))]
        public string MainCategoryName { get; set; } = default!;
        public ICollection<DetailedCategory>? DetailedCategories { get; set; }
        public ICollection<Item>? Items { get; set; }
    }
}