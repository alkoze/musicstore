﻿﻿using System;
 using System.ComponentModel.DataAnnotations;
 using Domain.Base;

namespace Domain.App
{
    public class Picture : DomainEntityIdMetadata
    {
        [Display(Name = nameof(ItemId),
            ResourceType = typeof(Resources.Views.Picture.Picture))]
        public Guid ItemId { get; set; } = default!;
        
        [Display(Name = nameof(Item),
            ResourceType = typeof(Resources.Views.Picture.Picture))]
        public Item? Item { get; set; }

        [Display(Name = nameof(PictureUrl),
            ResourceType = typeof(Resources.Views.Picture.Picture))]
        [MinLength(1)]
        [MaxLength(4056)]
        public string PictureUrl { get; set; } = default!;
    }
}