﻿﻿using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using Domain.Base;

namespace Domain.App
{
    public class ItemStatus : DomainEntityIdMetadata
    {
        [Display(Name = nameof(StatusName),
            ResourceType = typeof(Resources.Views.ItemStatus.ItemStatus))]
        public string StatusName { get; set; } = default!;
        public ICollection<Item>? Items { get; set; }
    }
}