﻿﻿using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using Domain.Base;

namespace Domain.App
{
    public class Location : DomainEntityIdMetadata
    {
        [Display(Name = nameof(LocationName),
            ResourceType = typeof(Resources.Views.Locations.Locations))]
        public string LocationName { get; set; } = default!;

        public ICollection<Item>? Items { get; set; }
    }
}