﻿﻿using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using Domain.Base;

namespace Domain.App
{
    public class DeliveryType : DomainEntityIdMetadata
    {
        [Display(Name = nameof(DeliveryTypeName),
            ResourceType = typeof(Resources.Views.DeliveryType.DeliveryType))]
        public string DeliveryTypeName { get; set; } = default!;
        public ICollection<Order>? Orders { get; set; }
        [Display(Name = nameof(DeliveryTypePrice),
            ResourceType = typeof(Resources.Views.DeliveryType.DeliveryType))]
        public int DeliveryTypePrice { get; set; } = default!;
    }
}