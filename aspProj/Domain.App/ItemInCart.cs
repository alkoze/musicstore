﻿using System;
using System.Collections.Generic;
using Domain.Base;

namespace Domain.App
{
    public class ItemInCart: DomainEntityIdMetadata
    {
        public Item? Item { get; set; }
        public Guid? ItemId { get; set; }
        public Cart? Cart { get; set; }
        public Guid? CartId { get; set; }
    }
}