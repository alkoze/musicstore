﻿﻿using System;
using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class Item : DomainEntityIdMetadataUser<AppUser>
    {
        [Display(Name = nameof(ItemName),
            ResourceType = typeof(Resources.Views.Items.Items))]
        [MinLength(1)]
        [MaxLength(256)]

        public string ItemName { get; set; } = default!;
        
        [Display(Name = nameof(YearMade),
            ResourceType = typeof(Resources.Views.Items.Items))]
        [Range(1, 2020)]
        public int? YearMade { get; set; }
        
        [Display(Name = nameof(ItemDescription),
            ResourceType = typeof(Resources.Views.Items.Items))]
        [MinLength(1)]
        [MaxLength(4056)]
        public string? ItemDescription { get; set; }
        
        [Display(Name = nameof(Price),
            ResourceType = typeof(Resources.Views.Items.Items))]
        public int Price { get; set; } = default!;
        
        [Display(Name = nameof(LocationId),
            ResourceType = typeof(Resources.Views.Items.Items))]

        public Guid LocationId { get; set; } = default!;
        [Display(Name = nameof(Location),
            ResourceType = typeof(Resources.Views.Items.Items))]
        public Location? Location { get; set; }
        
        [Display(Name = nameof(ItemQualityId),
            ResourceType = typeof(Resources.Views.Items.Items))]
        public Guid ItemQualityId { get; set; } = default!;
        [Display(Name = nameof(ItemQuality),
            ResourceType = typeof(Resources.Views.Items.Items))]
        public ItemQuality? ItemQuality { get; set; }
        [Display(Name = nameof(ItemStatus),
            ResourceType = typeof(Resources.Views.Items.Items))]

        public ItemStatus? ItemStatus { get; set; }
        
        [Display(Name = nameof(ItemStatusId),
            ResourceType = typeof(Resources.Views.Items.Items))]
        public Guid? ItemStatusId { get; set; }
        [Display(Name = nameof(DetailedCategory),
            ResourceType = typeof(Resources.Views.Items.Items))]

        public DetailedCategory? DetailedCategory { get; set; }
        
        [Display(Name = nameof(DetailedCategoryId),
            ResourceType = typeof(Resources.Views.Items.Items))]
        public Guid? DetailedCategoryId { get; set; }
        [Display(Name = nameof(MainCategory),
            ResourceType = typeof(Resources.Views.Items.Items))]

        public MainCategory? MainCategory { get; set; }
        
        [Display(Name = nameof(MainCategoryId),
            ResourceType = typeof(Resources.Views.Items.Items))]
        public Guid? MainCategoryId { get; set; }
        
        public Guid? OrderId { get; set; }
        public Order? Order { get; set; }

        public ICollection<ItemInCart>? ItemInCarts { get; set; }
        public ICollection<Picture>? Pictures { get; set; }
    }
}