﻿﻿using System.Collections.Generic;
 using System.ComponentModel.DataAnnotations;
 using Domain.Base;

namespace Domain.App
{
    public class ItemQuality : DomainEntityIdMetadata
    {
        [Display(Name = nameof(QualityName),
            ResourceType = typeof(Resources.Views.Qualities.Qualities))]
        public string QualityName { get; set; } = default!;
        public ICollection<Item>? Items { get; set; }
        
    }
}