﻿﻿using System;
 using System.ComponentModel.DataAnnotations;
 using Domain.App.Identity;
using Domain.Base;

namespace Domain.App
{
    public class Comment : DomainEntityIdMetadata
    {
        [Display(Name = nameof(CommentText),
            ResourceType = typeof(Resources.Views.Comment.Comment))]
        [MinLength(1)]
        [MaxLength(256)]
        public string CommentText { get; set; } = default!;
        [Display(Name = nameof(RatingId),
            ResourceType = typeof(Resources.Views.Comment.Comment))]
        public Guid? RatingId { get; set; }
        [Display(Name = nameof(Rating),
            ResourceType = typeof(Resources.Views.Comment.Comment))]
        public Rating? Rating { get; set; }
        [Display(Name = nameof(AppUserId),
            ResourceType = typeof(Resources.Views.Comment.Comment))]

        public Guid AppUserId { get; set; } = default!;
        [Display(Name = nameof(AppUser),
            ResourceType = typeof(Resources.Views.Comment.Comment))]
        public AppUser? AppUser { get; set; }

        public Guid Creator { get; set; } = default!;

    }
}