﻿﻿using ee.itcollege.alkoze.Contracts.DAL.Base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ICommentRepository: IBaseRepository<Comment>, ICommentRepositoryCustom
    {
        
    }
}