﻿﻿using System.Collections.Generic;
 using System.Threading.Tasks;
 using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IDetailedCategoryRepositoryCustom: IDetailedCategoryRepositoryCustom<DetailedCategoryView>
    {
        
    }

    public interface IDetailedCategoryRepositoryCustom<TDetailedCategoryView>
    {
        Task<IEnumerable<TDetailedCategoryView>> GetAllForViewAsync();
    }
}