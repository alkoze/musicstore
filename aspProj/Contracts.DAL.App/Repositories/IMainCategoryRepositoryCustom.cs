﻿﻿using System.Collections.Generic;
 using System.Threading.Tasks;
 using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IMainCategoryRepositoryCustom: IMainCategoryRepositoryCustom<MainCategory>
    {
        
    }

    public interface IMainCategoryRepositoryCustom<TMainCategory>
    {
        Task<IEnumerable<TMainCategory>> GetAllForViewAsync();
    }
}