﻿﻿using ee.itcollege.alkoze.Contracts.DAL.Base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IDeliveryTypeRepository: IBaseRepository<DeliveryType>, IDeliveryTypeRepositoryCustom
    {
        
    }
}