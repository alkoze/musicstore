﻿﻿using System;
 using System.Threading.Tasks;
 using ee.itcollege.alkoze.Contracts.DAL.Base.Repositories;
using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface ICartRepository: IBaseRepository<Cart>, ICartRepositoryCustom
    {
    }
}