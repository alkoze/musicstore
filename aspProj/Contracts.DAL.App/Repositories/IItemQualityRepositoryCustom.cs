﻿﻿using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IItemQualityRepositoryCustom: IItemQualityRepositoryCustom<ItemQuality>
    {
        
    }

    public interface IItemQualityRepositoryCustom<TItemQuality>
    {
        
    }
}