﻿﻿using System;
 using System.Collections.Generic;
 using System.Threading.Tasks;
 using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IItemRepositoryCustom: IItemRepositoryCustom<ItemView>
    {
        
    }

    public interface IItemRepositoryCustom<TItemView>
    {
        Task<IEnumerable<TItemView>> GetAllForViewAsync(Guid? id);
    }
}