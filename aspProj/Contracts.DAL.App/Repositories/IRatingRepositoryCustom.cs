﻿﻿using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IRatingRepositoryCustom: IRatingRepositoryCustom<Rating>
    {
        
    }

    public interface IRatingRepositoryCustom<TRating>
    {
        
    }
}