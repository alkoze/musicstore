﻿﻿using DAL.App.DTO;

namespace Contracts.DAL.App.Repositories
{
    public interface IItemStatusRepositoryCustom: IItemStatusRepositoryCustom<ItemStatus>
    {
        
    }

    public interface IItemStatusRepositoryCustom<TItemStatus>
    {
        
    }
}