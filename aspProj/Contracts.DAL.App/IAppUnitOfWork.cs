﻿using System;
using Contracts.DAL.App.Repositories;
using ee.itcollege.alkoze.Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork, IBaseEntityTracker
    {
        ICartRepository Carts { get; }
        ICommentRepository Comments { get; }
        IItemInCartRepository ItemInCarts { get; }
        IDeliveryTypeRepository DeliveryTypes { get; }
        IDetailedCategoryRepository DetailedCategories { get; }
        IItemQualityRepository ItemQualities { get; }
        IItemRepository Items { get; }
        IItemStatusRepository ItemStatuses { get; }
        ILocationRepository Locations { get; }
        IMainCategoryRepository MainCategories { get; }
        IOrderRepository Orders { get; }
        IPaymentMethodRepository PaymentMethods { get; }
        IPictureRepository Pictures { get; }
        IRatingRepository Ratings { get; }
        ILangStrRepository LangStrs { get; }
        ILangStrTranslationRepository LangStrTranslations { get; }
    }
}