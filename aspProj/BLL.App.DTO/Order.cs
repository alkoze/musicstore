﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace BLL.App.DTO
{
    public class Order : IDomainEntityId
    {
        public Guid Id { get; set; }
        public Guid? DeliveryTypeId { get; set; }
        public Guid AppUserId { get; set; }
        public int OrderPrice { get; set; }
        public string PaymentInfo { get; set; } = default!;

        public string DeliveryAddress { get; set; } = default!;
        
        public Guid PaymentMethodId { get; set; }

        public string PostalIndex { get; set; } = default!;
    }
}