﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace BLL.App.DTO
{
    public class DetailedCategory : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string DetailedCategoryName { get; set; } = default!;

        public Guid MainCategoryId { get; set; }
        public string MainCategory { get; set; } = default!;
    }
}