﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace BLL.App.DTO
{
    public class ItemStatus : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string StatusName { get; set; } = default!;
        public ICollection<ItemView>? Items { get; set; }
    }
}