﻿using System;
using Domain.Base;

namespace BLL.App.DTO
{
    public class ItemInCart: DomainEntityIdMetadata
    {
        public Item? Item { get; set; }
        public Guid? ItemId { get; set; }
        public Cart? Cart { get; set; }
        public Guid? CartId { get; set; }
    }
}