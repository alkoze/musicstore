﻿﻿using System;
using BLL.App.DTO.Identity;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;


namespace BLL.App.DTO
{
    public class Rating : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string RatingValue { get; set; } = default!;
    }
}