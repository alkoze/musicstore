﻿using System;
using System.Collections.Generic;

namespace BLL.App.DTO
{
    public class DetailedCategoryView
    {
        public Guid Id { get; set; }
        public string DetailedCategoryName { get; set; } = default!;

        public Guid MainCategoryId { get; set; }
        public string MainCategory { get; set; } = default!;
        
        public ICollection<Item>? Items { get; set; }
    }
}