﻿﻿using System;
using System.Collections.Generic;
 using System.Reflection;
 using BLL.App.DTO.Identity;
 using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace BLL.App.DTO
{
    public class Cart : IDomainEntityId
    {
        public Guid Id { get; set; }
        public ICollection<ItemView>? Items { get; set; }
        public Guid AppUserId { get; set; }
        public AppUser AppUser { get; set; } = default!;
        public int ItemCount { get; set; }
    }
}