﻿﻿using System;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace BLL.App.DTO
{
    public class Picture : IDomainEntityId
    {
        public Guid Id { get; set; }
        public Guid ItemId { get; set; }
        public Item? Item { get; set; }
        public string PictureUrl { get; set; } = default!;

    }
}