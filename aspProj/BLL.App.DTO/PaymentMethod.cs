﻿﻿using System;
using BLL.App.DTO.Identity;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace BLL.App.DTO
{
    public class PaymentMethod : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string PaymentMethodName { get; set; } = default!;
    }
}