﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.Contracts.DAL.Base;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace BLL.App.DTO
{
    public class ItemQuality : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string QualityName { get; set; } = default!;
        public ICollection<Item>? Items { get; set; }
    }
}