﻿using System;
using BLL.App.DTO.Identity;

namespace BLL.App.DTO
{
    public class CommentView
    {
        public Guid Id { get; set; }
        public string CommentText { get; set; } = default!;
        
        public Guid AppUserId { get; set; }
        public Guid? RatingId { get; set; }
        public string Rating { get; set; } = default!;
        
        public Guid Creator { get; set; } = default!;

    }
}