# SportMap ASP.NET front and backend

Project for: https://git.akaver.com/iti0213-2019s/course-materials/-/blob/master/homeworks/HW2.md    
Android location info: https://developer.android.com/reference/android/location/Location  
ERD schema: https://www.lucidchart.com/invitations/accept/9b0354f0-82c5-4cb3-a955-118fb31a43a2  
RESTful Api endpoints are testable and documented in url /swagger/  

## Database
~~~
"SqlServerConnection": "Server=localhost;User Id=SA;Password=xxxxx.yyyyyy;Database=sportmap;MultipleActiveResultSets=true"
~~~

## Install or update tooling
~~~
dotnet tool install --global dotnet-ef
dotnet tool update --global dotnet-ef

dotnet tool install -g dotnet-aspnet-codegenerator
dotnet tool update -g dotnet-aspnet-codegenerator
~~~

## Generate database migration
Run from solution folder.  
~~~
dotnet ef database drop --project DAL.App.EF --startup-project WebApp
dotnet ef migrations --project DAL.App.EF --startup-project WebApp add InitialDbCreation 
dotnet ef migrations --project DAL.App.EF --startup-project WebApp remove
dotnet ef database update --project DAL.App.EF --startup-project WebApp
~~~

## Enable nullable reference types
Add ***Directory.Build.props*** to solution folder for common project properties
~~~xml
<Project>
    <PropertyGroup>
        <LangVersion>latest</LangVersion>
        <Nullable>enable</Nullable>
    </PropertyGroup>
</Project>
~~~


## Generate identity UI
Install Microsoft.VisualStudio.Web.CodeGeneration.Design to WebApp.  
Run from inside the WebApp directory.  
~~~
dotnet aspnet-codegenerator identity -dc DAL.App.EF.AppDbContext -f  
~~~
After scaffolding do global search & replace:
~~~
</dd> => </dd>
asp-validation-summary="All" => asp-validation-summary="All"
just 4 commitasdasd
~~~ 

API launch in WebApp
========================================================
dotnet aspnet-codegenerator controller -name CartsController             -m Domain.App.Cart           -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name CommentsController      -m Domain.App.Comment     -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name DeliveryTypesController    -m Domain.App.DeliveryType    -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name DetailedCategoriesController         -m Domain.App.DetailedCategory        -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name ItemsController     -m Domain.App.Item    -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name ItemQualitiesController             -m Domain.App.ItemQuality          -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name ItemStatusesController             -m Domain.App.ItemStatus            -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name LocationsController             -m Domain.App.Location            -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name MainCategoriesController             -m Domain.App.MainCategory            -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name OrdersController             -m Domain.App.Order            -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name PaymentMethodsController             -m Domain.App.PaymentMethod            -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name PicturesController             -m Domain.App.Picture           -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name RatingsController             -m Domain.App.Rating            -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f
dotnet aspnet-codegenerator controller -name ItemInCartsController             -m Domain.App.ItemInCart          -actions -dc AppDbContext -outDir ApiControllers -api --useAsyncActions  -f


========================================================

View launch in WebApp
~~~
dotnet aspnet-codegenerator controller -name CartsController          -actions -m Domain.App.Cart          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name CommentsController          -actions -m Domain.App.Comment          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name DeliveryTypesController          -actions -m Domain.App.DeliveryType         -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name DetailedCategoriesController          -actions -m Domain.App.DetailedCategory          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name ItemsController          -actions -m Domain.App.Item          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name ItemQualitiesController          -actions -m Domain.App.ItemQuality          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name ItemStatusesController          -actions -m Domain.App.ItemStatus          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name LocationsController          -actions -m Domain.App.Location          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name MainCategoriesController          -actions -m Domain.App.MainCategory          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name OrdersController          -actions -m Domain.App.Order         -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name PaymentMethodsController          -actions -m Domain.App.PaymentMethod          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name PicturesController          -actions -m Domain.App.Picture          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name RatingsController          -actions -m Domain.App.Rating          -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f
dotnet aspnet-codegenerator controller -name ItemInCartsController          -actions -m Domain.App.ItemInCart         -dc AppDbContext -outDir Controllers --useDefaultLayout --useAsyncActions --referenceScriptLibraries -f


~~~
