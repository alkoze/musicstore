﻿﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace DAL.App.DTO
{
    public class LangStr : IDomainEntityId
    {
        public Guid Id { get; set; }

        [InverseProperty(nameof(Item.ItemName))]
        public ICollection<Item>? ItemNames { get; set; }

        [InverseProperty(nameof(Item.ItemDescription))]
        public ICollection<Item>? ItemDescriptions { get; set; }

        [InverseProperty(nameof(ItemQuality.QualityName))]
        public ICollection<ItemQuality>? ItemQualityNames { get; set; }

        [InverseProperty(nameof(MainCategory.MainCategoryName))]
        public ICollection<MainCategory>? MainCategoryNames { get; set; }

        [InverseProperty(nameof(DetailedCategory.DetailedCategoryName))] 
        public ICollection<DetailedCategory>? DetailedCategoryNames { get; set; }

        [InverseProperty(nameof(DeliveryType.DeliveryTypeName))]
        public ICollection<DeliveryType>? DeliveryTypeNames { get; set; }

    }
}