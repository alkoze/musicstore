﻿using System;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class CommentView
    {
        public Guid Id { get; set; }
        public string CommentText { get; set; } = default!;
        
        public Guid AppUserId { get; set; }
        public Guid? RatingId { get; set; }
        public string Rating { get; set; } = default!;
        
        public Guid Creator { get; set; } = default!;

    }
}