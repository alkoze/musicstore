﻿﻿using System;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class Comment : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string CommentText { get; set; } = default!;
        

        public Guid AppUserId { get; set; }
        public Guid? RatingId { get; set; }
        
        public Guid Creator { get; set; } = default!;


    }
}