﻿﻿using System;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;
using DAL.App.DTO.Identity;


namespace DAL.App.DTO
{
    public class Rating : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string RatingValue { get; set; } = default!;
    }
}