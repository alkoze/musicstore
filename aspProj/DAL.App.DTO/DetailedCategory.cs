﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace DAL.App.DTO
{
    public class DetailedCategory : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string DetailedCategoryName { get; set; } = default!;

        public Guid MainCategoryId { get; set; }
    }
}