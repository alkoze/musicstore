﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;
 using DAL.App.DTO.Identity;

 namespace DAL.App.DTO
{
    public class Cart : IDomainEntityId
    {
        public Guid Id { get; set; }
        public ICollection<ItemView>? Items { get; set; }
        public Guid AppUserId { get; set; }
        public AppUser AppUser { get; set; } = default!;

        public int ItemCount { get; set; }

    }
}