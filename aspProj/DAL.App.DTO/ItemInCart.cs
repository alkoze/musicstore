﻿using System;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace DAL.App.DTO
{
    public class ItemInCart: IDomainEntityId
    {
        public Item? Item { get; set; }
        public Guid? ItemId { get; set; }
        public Cart? Cart { get; set; }
        public Guid? CartId { get; set; }
        public Guid Id { get; set; }
    }
}