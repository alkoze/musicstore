﻿﻿using System;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;
using DAL.App.DTO.Identity;

namespace DAL.App.DTO
{
    public class PaymentMethod : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string PaymentMethodName { get; set; } = default!;
    }
}