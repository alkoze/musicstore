﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace DAL.App.DTO
{
    public class MainCategory : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string MainCategoryName { get; set; } = default!;
        public ICollection<DetailedCategory>? DetailedCategories { get; set; }
        public ICollection<Item>? Items { get; set; }
    }
}