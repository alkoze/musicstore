﻿﻿using System;
using System.Collections.Generic;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;
 using DAL.App.DTO.Identity;

 namespace DAL.App.DTO
{
    public class Item : IDomainEntityId
    {
        public Guid Id { get; set; }
        public string ItemName { get; set; } = default!;
        public int? YearMade { get; set; }
        public string? ItemDescription { get; set; }
        public int Price { get; set; } = default!;
        
        public Guid? OrderId { get; set; }

        public Guid? LocationId { get; set; }
        public Guid? ItemQualityId { get; set; }

        public Guid? ItemStatusId { get; set; }

        public Guid? DetailedCategoryId { get; set; }
        public Guid? MainCategoryId { get; set; }
        
        public Guid? AppUserId { get; set; }
        public AppUser? AppUser { get; set; }

    }
}