using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain.App;

namespace WebApp.Controllers
{
    /// <summary>
    /// DetailedCategoriesController api
    /// </summary>
    public class DetailedCategoriesController : Controller
    {
        private readonly AppDbContext _context;

        /// <summary>
        /// DetailedCategoriesController context
        /// </summary>
        /// <param name="context"></param>
        public DetailedCategoriesController(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// GET: DetailedCategories
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.DetailedCategories.Include(d => d.MainCategory);
            return View(await appDbContext.ToListAsync());
        }

        /// <summary>
        /// GET: DetailedCategories/Details/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detailedCategory = await _context.DetailedCategories
                .Include(d => d.MainCategory)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (detailedCategory == null)
            {
                return NotFound();
            }

            return View(detailedCategory);
        }

        /// <summary>
        /// GET: DetailedCategories/Create
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            ViewData["MainCategoryId"] = new SelectList(_context.MainCategories, "Id", "MainCategoryName");
            return View();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: DetailedCategories/Create
        /// </summary>
        /// <param name="detailedCategory"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DetailedCategoryName,MainCategoryId,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] DetailedCategory detailedCategory)
        {
            if (ModelState.IsValid)
            {
                detailedCategory.Id = Guid.NewGuid();
                _context.Add(detailedCategory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["MainCategoryId"] = new SelectList(_context.MainCategories, "Id", "MainCategoryName", detailedCategory.MainCategoryId);
            return View(detailedCategory);
        }

        /// <summary>
        /// GET: DetailedCategories/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detailedCategory = await _context.DetailedCategories.FindAsync(id);
            if (detailedCategory == null)
            {
                return NotFound();
            }
            ViewData["MainCategoryId"] = new SelectList(_context.MainCategories, "Id", "MainCategoryName", detailedCategory.MainCategoryId);
            return View(detailedCategory);
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: DetailedCategories/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="detailedCategory"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("DetailedCategoryName,MainCategoryId,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] DetailedCategory detailedCategory)
        {
            if (id != detailedCategory.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(detailedCategory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DetailedCategoryExists(detailedCategory.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["MainCategoryId"] = new SelectList(_context.MainCategories, "Id", "MainCategoryName", detailedCategory.MainCategoryId);
            return View(detailedCategory);
        }

        /// <summary>
        /// GET: DetailedCategories/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var detailedCategory = await _context.DetailedCategories
                .Include(d => d.MainCategory)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (detailedCategory == null)
            {
                return NotFound();
            }

            return View(detailedCategory);
        }

        /// <summary>
        /// POST: DetailedCategories/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var detailedCategory = await _context.DetailedCategories.FindAsync(id);
            _context.DetailedCategories.Remove(detailedCategory);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DetailedCategoryExists(Guid id)
        {
            return _context.DetailedCategories.Any(e => e.Id == id);
        }
    }
}
