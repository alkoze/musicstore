using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain.App;

namespace WebApp.Controllers
{
    /// <summary>
    /// ItemQualitiesController api
    /// </summary>
    public class ItemQualitiesController : Controller
    {
        private readonly AppDbContext _context;

        /// <summary>
        /// ItemQualitiesController context
        /// </summary>
        /// <param name="context"></param>
        public ItemQualitiesController(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// GET: ItemQualities
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.ItemQualities.ToListAsync());
        }

        /// <summary>
        /// GET: ItemQualities/Details/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemQuality = await _context.ItemQualities
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemQuality == null)
            {
                return NotFound();
            }

            return View(itemQuality);
        }

        /// <summary>
        /// GET: ItemQualities/Create
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: ItemQualities/Create
        /// </summary>
        /// <param name="itemQuality"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("QualityName,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] ItemQuality itemQuality)
        {
            if (ModelState.IsValid)
            {
                itemQuality.Id = Guid.NewGuid();
                _context.Add(itemQuality);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(itemQuality);
        }

        /// <summary>
        /// GET: ItemQualities/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemQuality = await _context.ItemQualities.FindAsync(id);
            if (itemQuality == null)
            {
                return NotFound();
            }
            return View(itemQuality);
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: ItemQualities/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="itemQuality"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("QualityName,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] ItemQuality itemQuality)
        {
            if (id != itemQuality.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(itemQuality);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemQualityExists(itemQuality.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(itemQuality);
        }

        /// <summary>
        /// GET: ItemQualities/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemQuality = await _context.ItemQualities
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemQuality == null)
            {
                return NotFound();
            }

            return View(itemQuality);
        }

        /// <summary>
        /// POST: ItemQualities/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var itemQuality = await _context.ItemQualities.FindAsync(id);
            _context.ItemQualities.Remove(itemQuality);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemQualityExists(Guid id)
        {
            return _context.ItemQualities.Any(e => e.Id == id);
        }
    }
}
