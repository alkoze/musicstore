using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain.App;

namespace WebApp.Controllers
{
    /// <summary>
    /// ItemInCartsController api
    /// </summary>
    public class ItemInCartsController : Controller
    {
        private readonly AppDbContext _context;

        /// <summary>
        /// ItemInCartsController context
        /// </summary>
        /// <param name="context"></param>
        public ItemInCartsController(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// GET: ItemInCarts
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.ItemInCarts.Include(i => i.Cart).Include(i => i.Item);
            return View(await appDbContext.ToListAsync());
        }

        /// <summary>
        /// GET: ItemInCarts/Details/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemInCart = await _context.ItemInCarts
                .Include(i => i.Cart)
                .Include(i => i.Item)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemInCart == null)
            {
                return NotFound();
            }

            return View(itemInCart);
        }

        /// <summary>
        /// GET: ItemInCarts/Create
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            ViewData["CartId"] = new SelectList(_context.Carts, "Id", "Id");
            ViewData["ItemId"] = new SelectList(_context.Items, "Id", "ItemName");
            return View();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: ItemInCarts/Create
        /// </summary>
        /// <param name="itemInCart"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ItemId,CartId,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] ItemInCart itemInCart)
        {
            if (ModelState.IsValid)
            {
                itemInCart.Id = Guid.NewGuid();
                _context.Add(itemInCart);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CartId"] = new SelectList(_context.Carts, "Id", "Id", itemInCart.CartId);
            ViewData["ItemId"] = new SelectList(_context.Items, "Id", "ItemName", itemInCart.ItemId);
            return View(itemInCart);
        }

        /// <summary>
        /// GET: ItemInCarts/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemInCart = await _context.ItemInCarts.FindAsync(id);
            if (itemInCart == null)
            {
                return NotFound();
            }
            ViewData["CartId"] = new SelectList(_context.Carts, "Id", "Id", itemInCart.CartId);
            ViewData["ItemId"] = new SelectList(_context.Items, "Id", "ItemName", itemInCart.ItemId);
            return View(itemInCart);
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: ItemInCarts/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="itemInCart"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("ItemId,CartId,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] ItemInCart itemInCart)
        {
            if (id != itemInCart.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(itemInCart);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemInCartExists(itemInCart.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CartId"] = new SelectList(_context.Carts, "Id", "Id", itemInCart.CartId);
            ViewData["ItemId"] = new SelectList(_context.Items, "Id", "ItemName", itemInCart.ItemId);
            return View(itemInCart);
        }

        /// <summary>
        /// GET: ItemInCarts/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemInCart = await _context.ItemInCarts
                .Include(i => i.Cart)
                .Include(i => i.Item)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemInCart == null)
            {
                return NotFound();
            }

            return View(itemInCart);
        }

        /// <summary>
        /// POST: ItemInCarts/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var itemInCart = await _context.ItemInCarts.FindAsync(id);
            _context.ItemInCarts.Remove(itemInCart);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemInCartExists(Guid id)
        {
            return _context.ItemInCarts.Any(e => e.Id == id);
        }
    }
}
