using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain.App;

namespace WebApp.Controllers
{
    /// <summary>
    /// ItemStatusesController api
    /// </summary>
    public class ItemStatusesController : Controller
    {
        private readonly AppDbContext _context;

        /// <summary>
        /// ItemStatusesController context
        /// </summary>
        /// <param name="context"></param>
        public ItemStatusesController(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// GET: ItemStatuses
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.ItemStatuses.ToListAsync());
        }

        /// <summary>
        /// GET: ItemStatuses/Details/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemStatus = await _context.ItemStatuses
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemStatus == null)
            {
                return NotFound();
            }

            return View(itemStatus);
        }

        /// <summary>
        /// GET: ItemStatuses/Create
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: ItemStatuses/Create
        /// </summary>
        /// <param name="itemStatus"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StatusName,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] ItemStatus itemStatus)
        {
            if (ModelState.IsValid)
            {
                itemStatus.Id = Guid.NewGuid();
                _context.Add(itemStatus);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(itemStatus);
        }

        /// <summary>
        /// GET: ItemStatuses/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemStatus = await _context.ItemStatuses.FindAsync(id);
            if (itemStatus == null)
            {
                return NotFound();
            }
            return View(itemStatus);
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: ItemStatuses/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="itemStatus"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("StatusName,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] ItemStatus itemStatus)
        {
            if (id != itemStatus.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(itemStatus);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemStatusExists(itemStatus.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(itemStatus);
        }

        /// <summary>
        /// GET: ItemStatuses/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var itemStatus = await _context.ItemStatuses
                .FirstOrDefaultAsync(m => m.Id == id);
            if (itemStatus == null)
            {
                return NotFound();
            }

            return View(itemStatus);
        }

        /// <summary>
        /// POST: ItemStatuses/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var itemStatus = await _context.ItemStatuses.FindAsync(id);
            _context.ItemStatuses.Remove(itemStatus);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemStatusExists(Guid id)
        {
            return _context.ItemStatuses.Any(e => e.Id == id);
        }
    }
}
