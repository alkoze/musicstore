using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain.App;

namespace WebApp.Controllers
{
    /// <summary>
    /// ItemsController api
    /// </summary>
    public class ItemsController : Controller
    {
        private readonly AppDbContext _context;

        /// <summary>
        /// ItemsController context
        /// </summary>
        /// <param name="context"></param>
        public ItemsController(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// GET: Items
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var appDbContext = _context.Items.Include(i => i.AppUser).Include(i => i.DetailedCategory).Include(i => i.ItemQuality).Include(i => i.ItemStatus).Include(i => i.Location).Include(i => i.MainCategory);
            return View(await appDbContext.ToListAsync());
        }

        /// <summary>
        /// GET: Items/Details/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items
                .Include(i => i.AppUser)
                .Include(i => i.DetailedCategory)
                .Include(i => i.ItemQuality)
                .Include(i => i.ItemStatus)
                .Include(i => i.Location)
                .Include(i => i.MainCategory)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        /// <summary>
        /// GET: Items/Create
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "FirstName");
            ViewData["DetailedCategoryId"] = new SelectList(_context.DetailedCategories, "Id", "DetailedCategoryName");
            ViewData["ItemQualityId"] = new SelectList(_context.ItemQualities, "Id", "QualityName");
            ViewData["ItemStatusId"] = new SelectList(_context.ItemStatuses, "Id", "StatusName");
            ViewData["LocationId"] = new SelectList(_context.Locations, "Id", "LocationName");
            ViewData["MainCategoryId"] = new SelectList(_context.MainCategories, "Id", "MainCategoryName");
            return View();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: Items/Create
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ItemName,YearMade,ItemDescription,Price,LocationId,ItemQualityId,ItemStatusId,DetailedCategoryId,MainCategoryId,AppUserId,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] Item item)
        {
            if (ModelState.IsValid)
            {
                item.Id = Guid.NewGuid();
                _context.Add(item);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "FirstName", item.AppUserId);
            ViewData["DetailedCategoryId"] = new SelectList(_context.DetailedCategories, "Id", "DetailedCategoryName", item.DetailedCategoryId);
            ViewData["ItemQualityId"] = new SelectList(_context.ItemQualities, "Id", "QualityName", item.ItemQualityId);
            ViewData["ItemStatusId"] = new SelectList(_context.ItemStatuses, "Id", "StatusName", item.ItemStatusId);
            ViewData["LocationId"] = new SelectList(_context.Locations, "Id", "LocationName", item.LocationId);
            ViewData["MainCategoryId"] = new SelectList(_context.MainCategories, "Id", "MainCategoryName", item.MainCategoryId);
            return View(item);
        }

        /// <summary>
        /// GET: Items/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items.FindAsync(id);
            if (item == null)
            {
                return NotFound();
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "FirstName", item.AppUserId);
            ViewData["DetailedCategoryId"] = new SelectList(_context.DetailedCategories, "Id", "DetailedCategoryName", item.DetailedCategoryId);
            ViewData["ItemQualityId"] = new SelectList(_context.ItemQualities, "Id", "QualityName", item.ItemQualityId);
            ViewData["ItemStatusId"] = new SelectList(_context.ItemStatuses, "Id", "StatusName", item.ItemStatusId);
            ViewData["LocationId"] = new SelectList(_context.Locations, "Id", "LocationName", item.LocationId);
            ViewData["MainCategoryId"] = new SelectList(_context.MainCategories, "Id", "MainCategoryName", item.MainCategoryId);
            return View(item);
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// POST: Items/Edit/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("ItemName,YearMade,ItemDescription,Price,LocationId,ItemQualityId,ItemStatusId,DetailedCategoryId,MainCategoryId,AppUserId,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] Item item)
        {
            if (id != item.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(item);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ItemExists(item.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AppUserId"] = new SelectList(_context.Users, "Id", "FirstName", item.AppUserId);
            ViewData["DetailedCategoryId"] = new SelectList(_context.DetailedCategories, "Id", "DetailedCategoryName", item.DetailedCategoryId);
            ViewData["ItemQualityId"] = new SelectList(_context.ItemQualities, "Id", "QualityName", item.ItemQualityId);
            ViewData["ItemStatusId"] = new SelectList(_context.ItemStatuses, "Id", "StatusName", item.ItemStatusId);
            ViewData["LocationId"] = new SelectList(_context.Locations, "Id", "LocationName", item.LocationId);
            ViewData["MainCategoryId"] = new SelectList(_context.MainCategories, "Id", "MainCategoryName", item.MainCategoryId);
            return View(item);
        }

        /// <summary>
        /// GET: Items/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = await _context.Items
                .Include(i => i.AppUser)
                .Include(i => i.DetailedCategory)
                .Include(i => i.ItemQuality)
                .Include(i => i.ItemStatus)
                .Include(i => i.Location)
                .Include(i => i.MainCategory)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (item == null)
            {
                return NotFound();
            }

            return View(item);
        }

        /// <summary>
        /// POST: Items/Delete/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var item = await _context.Items.FindAsync(id);
            _context.Items.Remove(item);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ItemExists(Guid id)
        {
            return _context.Items.Any(e => e.Id == id);
        }
    }
}
