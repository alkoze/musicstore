using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using DAL.App.EF;
using Domain.App;

namespace WebApp.Controllers
{
    /// <summary>
    /// DeliveryTypesController api
    /// </summary>
    public class DeliveryTypesController : Controller
    {
        private readonly AppDbContext _context;

        /// <summary>
        /// DeliveryTypesController context
        /// </summary>
        /// <param name="context"></param>
        public DeliveryTypesController(AppDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// DeliveryTypes
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            return View(await _context.DeliveryTypes.ToListAsync());
        }

        /// <summary>
        /// DeliveryTypes/Details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryType = await _context.DeliveryTypes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deliveryType == null)
            {
                return NotFound();
            }

            return View(deliveryType);
        }

        /// <summary>
        /// DeliveryTypes/Create
        /// </summary>
        /// <returns></returns>
        public IActionResult Create()
        {
            return View();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// DeliveryTypes/Create
        /// </summary>
        /// <param name="deliveryType"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("DeliveryTypeName,DeliveryTypePrice,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] DeliveryType deliveryType)
        {
            if (ModelState.IsValid)
            {
                deliveryType.Id = Guid.NewGuid();
                _context.Add(deliveryType);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(deliveryType);
        }

        /// <summary>
        /// DeliveryTypes/Edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryType = await _context.DeliveryTypes.FindAsync(id);
            if (deliveryType == null)
            {
                return NotFound();
            }
            return View(deliveryType);
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        /// <summary>
        /// DeliveryTypes/Edit/
        /// </summary>
        /// <param name="id"></param>
        /// <param name="deliveryType"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Guid id, [Bind("DeliveryTypeName,DeliveryTypePrice,CreatedBy,CreatedAt,ChangedBy,ChangedAt,Id")] DeliveryType deliveryType)
        {
            if (id != deliveryType.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(deliveryType);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DeliveryTypeExists(deliveryType.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(deliveryType);
        }

        /// <summary>
        /// DeliveryTypes/Delete
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var deliveryType = await _context.DeliveryTypes
                .FirstOrDefaultAsync(m => m.Id == id);
            if (deliveryType == null)
            {
                return NotFound();
            }

            return View(deliveryType);
        }

        /// <summary>
        /// DeliveryTypes/Delete/
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var deliveryType = await _context.DeliveryTypes.FindAsync(id);
            _context.DeliveryTypes.Remove(deliveryType);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool DeliveryTypeExists(Guid id)
        {
            return _context.DeliveryTypes.Any(e => e.Id == id);
        }
    }
}
