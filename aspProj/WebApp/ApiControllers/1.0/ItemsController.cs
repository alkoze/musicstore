using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Extensions;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// ItemsController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class ItemsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly ItemMapper _mapper = new ItemMapper();
        /// <summary>
        /// Constructor
        /// </summary>
        public ItemsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined Item collection.
        /// </summary>
        /// <returns>List of available Items</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Item>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Item>>> GetItems()
        {
            var result = await _bll.Items.GetAllForViewAsync(null);
            return Ok(result.Select(e => _mapper.MapItemView(e)));
        }

        /// <summary>
        /// Get single Item
        /// </summary>
        /// <param name="id">Item Id</param>
        /// <returns>request Item</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Item))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Item>> GetItem(Guid id)
        {
            var item = await _bll.Items.GetAllForViewAsync(id);

            if (item == null)
            {
                return NotFound(new V1DTO.MessageDTO("Item not found"));
            }

            return Ok(item.Select(e => _mapper.MapItemView(e)).FirstOrDefault());
        }

        /// <summary>
        /// Update the Item
        /// </summary>
        /// <param name="id">Item id</param>
        /// <param name="item">Item object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutItem(Guid id, V1DTO.Item item)
        {
            if (id != item.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and Item.id do not match"));
            }

            await _bll.Items.UpdateAsync(_mapper.Map(item));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Item
        /// </summary>
        /// <param name="item">Item object</param>
        /// <returns>created Item object</returns>
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Item))]
        public async Task<ActionResult<V1DTO.Item>> PostItem(
            V1DTO.Item item)
        {
            var bllEntity = _mapper.Map(item);
            _bll.Items.Add(bllEntity);
            await _bll.SaveChangesAsync();
            item.Id = bllEntity.Id;
            return CreatedAtAction("GetItem",
                new {id = item.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                item);
        }

        /// <summary>
        /// Delete the Item
        /// </summary>
        /// <param name="id">Item Id</param>
        /// <returns>deleted Item object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Item))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Item>> DeleteItem(Guid id)
        {
            var item = await _bll.Items.FirstOrDefaultAsync(id);
            if (item == null)
            {
                return NotFound(new V1DTO.MessageDTO("Item not found"));
            }

            await _bll.Items.RemoveAsync(item);
            await _bll.SaveChangesAsync();

            return Ok(item);
        }
    }
}