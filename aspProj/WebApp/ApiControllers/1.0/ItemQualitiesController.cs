using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// ItemQualitiesController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class ItemQualitiesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly ItemQualityMapper _mapper = new ItemQualityMapper();

       /// <summary>
        /// Constructor
        /// </summary>
        public ItemQualitiesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined ItemQuality collection.
        /// </summary>
        /// <returns>List of available ItemQualitys</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.ItemQuality>))]
        public async Task<ActionResult<IEnumerable<V1DTO.ItemQuality>>> GetItemQualities()
        {
            return Ok((await _bll.ItemQualities.GetAllAsync()).Select(e => _mapper.Map(e)));
        }

        /// <summary>
        /// Get single ItemQuality
        /// </summary>
        /// <param name="id">ItemQuality Id</param>
        /// <returns>request ItemQuality</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.ItemQuality))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.ItemQuality>> GetItemQuality(Guid id)
        {
            var itemQuality = await _bll.ItemQualities.FirstOrDefaultAsync(id);

            if (itemQuality == null)
            {
                return NotFound(new V1DTO.MessageDTO("ItemQuality not found"));
            }

            return Ok(_mapper.Map(itemQuality));
        }

        /// <summary>
        /// Update the ItemQuality
        /// </summary>
        /// <param name="id">ItemQuality id</param>
        /// <param name="itemQuality">ItemQuality object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutItemQuality(Guid id, V1DTO.ItemQuality itemQuality)
        {
            if (id != itemQuality.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and ItemQuality.id do not match"));
            }

            await _bll.ItemQualities.UpdateAsync(_mapper.Map(itemQuality));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new ItemQuality
        /// </summary>
        /// <param name="itemQuality">ItemQuality object</param>
        /// <returns>created ItemQuality object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.ItemQuality))]
        public async Task<ActionResult<V1DTO.ItemQuality>> PostItemQuality(
            V1DTO.ItemQuality itemQuality)
        {
            var bllEntity = _mapper.Map(itemQuality);
            _bll.ItemQualities.Add(bllEntity);
            await _bll.SaveChangesAsync();
            itemQuality.Id = bllEntity.Id;

            return CreatedAtAction("GetItemQuality",
                new {id = itemQuality.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                itemQuality);
        }

        /// <summary>
        /// Delete the ItemQuality
        /// </summary>
        /// <param name="id">ItemQuality Id</param>
        /// <returns>deleted ItemQuality object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.ItemQuality))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.ItemQuality>> DeleteItemQuality(Guid id)
        {
            var itemQuality = await _bll.ItemQualities.FirstOrDefaultAsync(id);
            if (itemQuality == null)
            {
                return NotFound(new V1DTO.MessageDTO("ItemQuality not found"));
            }

            await _bll.ItemQualities.RemoveAsync(itemQuality);
            await _bll.SaveChangesAsync();

            return Ok(itemQuality);
        }
    }
}