using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// CommentsController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class CommentsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly CommentMapper _mapper = new CommentMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public CommentsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined Comment collection.
        /// </summary>
        /// <returns>List of available Comments</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Comment>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Comment>>> GetComments()
        {
            var result = await _bll.Comments.GetAllForViewAsync();
            return Ok(result.Select(e => _mapper.MapCommentView(e)));
        }

        /// <summary>
        /// Get single Comment
        /// </summary>
        /// <param name="id">Comment Id</param>
        /// <returns>request Comment</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Comment))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Comment>> GetComment(Guid id)
        {
            var comment = await _bll.Comments.FirstOrDefaultAsync(id);

            if (comment == null)
            {
                return NotFound(new V1DTO.MessageDTO("Comment not found"));
            }

            return Ok(_mapper.Map(comment));
        }

        /// <summary>
        /// Update the Comment
        /// </summary>
        /// <param name="id">Comment id</param>
        /// <param name="comment">Comment object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutComment(Guid id, V1DTO.Comment comment)
        {
            if (id != comment.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and Comment.id do not match"));
            }

            await _bll.Comments.UpdateAsync(_mapper.Map(comment));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Comment
        /// </summary>
        /// <param name="comment">Comment object</param>
        /// <returns>created Comment object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Comment))]
        public async Task<ActionResult<V1DTO.Comment>> PostComment(
            V1DTO.Comment comment)
        {
            var bllEntity = _mapper.Map(comment);
            _bll.Comments.Add(bllEntity);
            await _bll.SaveChangesAsync();
            comment.Id = bllEntity.Id;

            return CreatedAtAction("GetComment",
                new {id = comment.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                comment);
        }

        /// <summary>
        /// Delete the Comment
        /// </summary>
        /// <param name="id">Comment Id</param>
        /// <returns>deleted Comment object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Comment))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Comment>> DeleteComment(Guid id)
        {
            var comment = await _bll.Comments.FirstOrDefaultAsync(id);
            if (comment == null)
            {
                return NotFound(new V1DTO.MessageDTO("Comment not found"));
            }

            await _bll.Comments.RemoveAsync(comment);
            await _bll.SaveChangesAsync();

            return Ok(comment);
        }
    }
}