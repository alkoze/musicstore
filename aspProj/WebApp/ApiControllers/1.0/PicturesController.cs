using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// PicturesController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class PicturesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly PictureMapper _mapper = new PictureMapper();
       /// <summary>
        /// Constructor
        /// </summary>
        public PicturesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined Picture collection.
        /// </summary>
        /// <returns>List of available Pictures</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Picture>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Picture>>> GetPictures()
        {
            return Ok((await _bll.Pictures.GetAllAsync()).Select(e => _mapper.Map(e)));
        }

        /// <summary>
        /// Get single Picture
        /// </summary>
        /// <param name="id">Picture Id</param>
        /// <returns>request Picture</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Picture))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Picture>> GetPicture(Guid id)
        {
            var picture = await _bll.Pictures.FirstOrDefaultAsync(id);

            if (picture == null)
            {
                return NotFound(new V1DTO.MessageDTO("Picture not found"));
            }

            return Ok(_mapper.Map(picture));
        }

        /// <summary>
        /// Update the Picture
        /// </summary>
        /// <param name="id">Picture id</param>
        /// <param name="picture">Picture object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutPicture(Guid id, V1DTO.Picture picture)
        {
            if (id != picture.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and Picture.id do not match"));
            }

            await _bll.Pictures.UpdateAsync(_mapper.Map(picture));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Picture
        /// </summary>
        /// <param name="picture">Picture object</param>
        /// <returns>created Picture object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Picture))]
        public async Task<ActionResult<V1DTO.Picture>> PostPicture(
            V1DTO.Picture picture)
        {
            var bllEntity = _mapper.Map(picture);
            _bll.Pictures.Add(bllEntity);
            await _bll.SaveChangesAsync();
            picture.Id = bllEntity.Id;

            return CreatedAtAction("GetPicture",
                new {id = picture.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                picture);
        }

        /// <summary>
        /// Delete the Picture
        /// </summary>
        /// <param name="id">Picture Id</param>
        /// <returns>deleted Picture object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Picture))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Picture>> DeletePicture(Guid id)
        {
            var picture = await _bll.Pictures.FirstOrDefaultAsync(id);
            if (picture == null)
            {
                return NotFound(new V1DTO.MessageDTO("Picture not found"));
            }

            await _bll.Pictures.RemoveAsync(picture);
            await _bll.SaveChangesAsync();

            return Ok(picture);
        }
    }
}