using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// LocationsController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class LocationsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly LocationMapper _mapper = new LocationMapper();
        /// <summary>
        /// Constructor
        /// </summary>
        public LocationsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined Location collection.
        /// </summary>
        /// <returns>List of available Locations</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Location>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Location>>> GetLocation()
        {
            return Ok((await _bll.Locations.GetAllAsync()).Select(e => _mapper.Map(e)));
        }

        /// <summary>
        /// Get single Location
        /// </summary>
        /// <param name="id">Location Id</param>
        /// <returns>request Location</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Location))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Location>> GetLocation(Guid id)
        {
            var location = await _bll.Locations.FirstOrDefaultAsync(id);

            if (location == null)
            {
                return NotFound(new V1DTO.MessageDTO("Location not found"));
            }

            return Ok(_mapper.Map(location));
        }

        /// <summary>
        /// Update the Location
        /// </summary>
        /// <param name="id">Location id</param>
        /// <param name="location">Location object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutLocation(Guid id, V1DTO.Location location)
        {
            if (id != location.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and Location.id do not match"));
            }

            await _bll.Locations.UpdateAsync(_mapper.Map(location));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Location
        /// </summary>
        /// <param name="location">Location object</param>
        /// <returns>created Location object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Location))]
        public async Task<ActionResult<V1DTO.Location>> PostLocation(
            V1DTO.Location location)
        {
            var bllEntity = _mapper.Map(location);
            _bll.Locations.Add(bllEntity);
            await _bll.SaveChangesAsync();
            location.Id = bllEntity.Id;

            return CreatedAtAction("GetLocation",
                new {id = location.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                location);
        }

        /// <summary>
        /// Delete the Location
        /// </summary>
        /// <param name="id">Location Id</param>
        /// <returns>deleted Location object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Location))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Location>> DeleteLocation(Guid id)
        {
            var location = await _bll.Locations.FirstOrDefaultAsync(id);
            if (location == null)
            {
                return NotFound(new V1DTO.MessageDTO("Location not found"));
            }

            await _bll.Locations.RemoveAsync(location);
            await _bll.SaveChangesAsync();

            return Ok(location);
        }
    }
}