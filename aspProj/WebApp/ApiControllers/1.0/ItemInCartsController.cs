using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// ItemInCartsController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class ItemInCartsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly ItemInCartMapper _mapper = new ItemInCartMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public ItemInCartsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined ItemInCart collection.
        /// </summary>
        /// <returns>List of available ItemInCarts</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.ItemInCart>))]
        public async Task<ActionResult<IEnumerable<V1DTO.ItemInCart>>> GetItemInCarts()
        {
            return Ok((await _bll.ItemInCarts.GetAllAsync()).Select(e => _mapper.Map(e)));
        }

        /// <summary>
        /// Get single ItemInCart
        /// </summary>
        /// <param name="id">ItemInCart Id</param>
        /// <returns>request ItemInCart</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.ItemInCart))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.ItemInCart>> GetItemInCart(Guid id)
        {
            var itemInCart = await _bll.ItemInCarts.FirstOrDefaultAsync(id);

            if (itemInCart == null)
            {
                return NotFound(new V1DTO.MessageDTO("ItemInCart not found"));
            }

            return Ok(_mapper.Map(itemInCart));
        }

        /// <summary>
        /// Update the ItemInCart
        /// </summary>
        /// <param name="id">ItemInCart id</param>
        /// <param name="itemInCart">ItemInCart object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutItemInCart(Guid id, V1DTO.ItemInCart itemInCart)
        {
            if (id != itemInCart.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and ItemInCart.id do not match"));
            }
            await _bll.ItemInCarts.UpdateAsync(_mapper.Map(itemInCart));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new ItemInCart
        /// </summary>
        /// <param name="itemInCart">ItemInCart object</param>
        /// <returns>created ItemInCart object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.ItemInCart))]
        public async Task<ActionResult<V1DTO.ItemInCart>> PostItemInCart(
            V1DTO.ItemInCart itemInCart)
        {
            var bllEntity = _mapper.Map(itemInCart);
            _bll.ItemInCarts.Add(bllEntity);
            await _bll.SaveChangesAsync();
            itemInCart.Id = bllEntity.Id;

            return CreatedAtAction("GetItemInCart",
                new {id = itemInCart.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                itemInCart);
        }

        /// <summary>
        /// Delete the ItemInCart
        /// </summary>
        /// <param name="id">ItemInCart Id</param>
        /// <returns>deleted ItemInCart object</returns>
        [AllowAnonymous]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.ItemInCart))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.ItemInCart>> DeleteItemInCart(Guid id)
        {
            var itemInCart = await _bll.ItemInCarts.FirstOrDefaultAsync(id);
            if (itemInCart == null)
            {
                return NotFound(new V1DTO.MessageDTO("ItemInCart not found"));
            }

            await _bll.ItemInCarts.RemoveAsync(itemInCart);
            await _bll.SaveChangesAsync();

            return Ok(itemInCart);
        }
    }
}