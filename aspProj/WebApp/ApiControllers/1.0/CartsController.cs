using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// CartsController Api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class CartsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly CartMapper _mapper = new CartMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public CartsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined Cart collection.
        /// </summary>
        /// <returns>List of available Carts</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Cart>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Cart>>> GetCarts()
        {
            await _bll.Carts.UpdateItemCount(null);
            var result = await _bll.Carts.GetAllForViewAsync(null);
            return Ok(result.Select(e => _mapper.MapCart(e)));
        }

        /// <summary>
        /// Get single Cart
        /// </summary>
        /// <param name="id">Cart Id</param>
        /// <returns>request Cart</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Cart))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Cart>> GetCart(Guid id)
        {
            var cart = await _bll.Carts.FirstOrDefaultAsync(id);
            await _bll.Carts.UpdateItemCount(id);

            if (cart == null)
            {
                return NotFound(new V1DTO.MessageDTO("Cart not found"));
            }

            return Ok(_mapper.Map(cart));
        }

        /// <summary>
        /// Update the Cart
        /// </summary>
        /// <param name="id">Cart id</param>
        /// <param name="cart">Cart object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutCart(Guid id, V1DTO.Cart cart)
        {
            if (id != cart.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and Cart.id do not match"));
            }
            await _bll.Carts.UpdateAsync(_mapper.Map(cart));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Cart
        /// </summary>
        /// <param name="cart">Cart object</param>
        /// <returns>created Cart object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Cart))]
        public async Task<ActionResult<V1DTO.Cart>> PostCart(
            V1DTO.Cart cart)
        {
            var bllEntity = _mapper.Map(cart);
            _bll.Carts.Add(bllEntity);
            await _bll.SaveChangesAsync();
            cart.Id = bllEntity.Id;

            return CreatedAtAction("GetCart",
                new {id = cart.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                cart);
        }

        /// <summary>
        /// Delete the Cart
        /// </summary>
        /// <param name="id">Cart Id</param>
        /// <returns>deleted Cart object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Cart))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Cart>> DeleteCart(Guid id)
        {
            var cart = await _bll.Carts.FirstOrDefaultAsync(id);
            if (cart == null)
            {
                return NotFound(new V1DTO.MessageDTO("Cart not found"));
            }

            await _bll.Carts.RemoveAsync(cart);
            await _bll.SaveChangesAsync();
            return Ok(cart);
        }
    }
}