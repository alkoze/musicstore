using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;

namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// RatingsController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class RatingsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly RatingMapper _mapper = new RatingMapper();
        /// <summary>
        /// Constructor
        /// </summary>
        public RatingsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined Rating collection.
        /// </summary>
        /// <returns>List of available Ratings</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.Rating>))]
        public async Task<ActionResult<IEnumerable<V1DTO.Rating>>> GetRatings()
        {
            return Ok((await _bll.Ratings.GetAllAsync()).Select(e => _mapper.Map(e)));
        }

        /// <summary>
        /// Get single Rating
        /// </summary>
        /// <param name="id">Rating Id</param>
        /// <returns>request Rating</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Rating))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Rating>> GetRating(Guid id)
        {
            var rating = await _bll.Ratings.FirstOrDefaultAsync(id);

            if (rating == null)
            {
                return NotFound(new V1DTO.MessageDTO("Rating not found"));
            }

            return Ok(_mapper.Map(rating));
        }

        /// <summary>
        /// Update the Rating
        /// </summary>
        /// <param name="id">Rating id</param>
        /// <param name="rating">Rating object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutRating(Guid id, V1DTO.Rating rating)
        {
            if (id != rating.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and Rating.id do not match"));
            }

            await _bll.Ratings.UpdateAsync(_mapper.Map(rating));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new Rating
        /// </summary>
        /// <param name="rating">Rating object</param>
        /// <returns>created Rating object</returns>
        [AllowAnonymous]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.Rating))]
        public async Task<ActionResult<V1DTO.Rating>> PostRating(
            V1DTO.Rating rating)
        {
            var bllEntity = _mapper.Map(rating);
            _bll.Ratings.Add(bllEntity);
            await _bll.SaveChangesAsync();
            rating.Id = bllEntity.Id;

            return CreatedAtAction("GetRating",
                new {id = rating.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                rating);
        }

        /// <summary>
        /// Delete the Rating
        /// </summary>
        /// <param name="id">Rating Id</param>
        /// <returns>deleted Rating object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.Rating))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.Rating>> DeleteRating(Guid id)
        {
            var rating = await _bll.Ratings.FirstOrDefaultAsync(id);
            if (rating == null)
            {
                return NotFound(new V1DTO.MessageDTO("Rating not found"));
            }

            await _bll.Ratings.RemoveAsync(rating);
            await _bll.SaveChangesAsync();

            return Ok(rating);
        }
    }
}