using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// MainCategoriesController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class MainCategoriesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly MainCategoryMapper _mapper = new MainCategoryMapper();
        /// <summary>
        /// Constructor
        /// </summary>
        public MainCategoriesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined MainCategory collection.
        /// </summary>
        /// <returns>List of available MainCategorys</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.MainCategory>))]
        public async Task<ActionResult<IEnumerable<V1DTO.MainCategory>>> GetMainCategories()
        {
            var result = await _bll.MainCategories.GetAllForViewAsync();
            return Ok(result.Select(e => _mapper.MapMainCategory(e)));
        }

        /// <summary>
        /// Get single MainCategory
        /// </summary>
        /// <param name="id">MainCategory Id</param>
        /// <returns>request MainCategory</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.MainCategory))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.MainCategory>> GetMainCategory(Guid id)
        {
            var mainCategory = await _bll.MainCategories.FirstOrDefaultAsync(id);

            if (mainCategory == null)
            {
                return NotFound(new V1DTO.MessageDTO("MainCategory not found"));
            }

            return Ok(_mapper.Map(mainCategory));
        }

        /// <summary>
        /// Update the MainCategory
        /// </summary>
        /// <param name="id">MainCategory id</param>
        /// <param name="mainCategory">MainCategory object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutMainCategory(Guid id, V1DTO.MainCategory mainCategory)
        {
            if (id != mainCategory.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and MainCategory.id do not match"));
            }

            await _bll.MainCategories.UpdateAsync(_mapper.Map(mainCategory));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new MainCategory
        /// </summary>
        /// <param name="mainCategory">MainCategory object</param>
        /// <returns>created MainCategory object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.MainCategory))]
        public async Task<ActionResult<V1DTO.MainCategory>> PostMainCategory(
            V1DTO.MainCategory mainCategory)
        {
            var bllEntity = _mapper.Map(mainCategory);
            _bll.MainCategories.Add(bllEntity);
            await _bll.SaveChangesAsync();
            mainCategory.Id = bllEntity.Id;

            return CreatedAtAction("GetMainCategory",
                new {id = mainCategory.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                mainCategory);
        }

        /// <summary>
        /// Delete the MainCategory
        /// </summary>
        /// <param name="id">MainCategory Id</param>
        /// <returns>deleted MainCategory object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.MainCategory))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.MainCategory>> DeleteMainCategory(Guid id)
        {
            var mainCategory = await _bll.MainCategories.FirstOrDefaultAsync(id);
            if (mainCategory == null)
            {
                return NotFound(new V1DTO.MessageDTO("MainCategory not found"));
            }

            await _bll.MainCategories.RemoveAsync(mainCategory);
            await _bll.SaveChangesAsync();

            return Ok(mainCategory);
        }
    }
}