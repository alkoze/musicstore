using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// DetailedCategoriesController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class DetailedCategoriesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly DetailedCategoryMapper _mapper = new DetailedCategoryMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public DetailedCategoriesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined DetailedCategory collection.
        /// </summary>
        /// <returns>List of available DetailedCategorys</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.DetailedCategory>))]
        public async Task<ActionResult<IEnumerable<V1DTO.DetailedCategory>>> GetDetailedCategory()
        {
            var result = await _bll.DetailedCategories.GetAllForViewAsync();
            return Ok(result.Select(e => _mapper.MapDetailedCategoryView(e)));
        }

        /// <summary>
        /// Get single DetailedCategory
        /// </summary>
        /// <param name="id">DetailedCategory Id</param>
        /// <returns>request DetailedCategory</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.DetailedCategory))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.DetailedCategory>> GetDetailedCategory(Guid id)
        {
            var detailedCategory = await _bll.DetailedCategories.FirstOrDefaultAsync(id);

            if (detailedCategory == null)
            {
                return NotFound(new V1DTO.MessageDTO("DetailedCategory not found"));
            }

            return Ok(_mapper.Map(detailedCategory));
        }

        /// <summary>
        /// Update the DetailedCategory
        /// </summary>
        /// <param name="id">DetailedCategory id</param>
        /// <param name="detailedCategory">DetailedCategory object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutDetailedCategory(Guid id, V1DTO.DetailedCategory detailedCategory)
        {
            if (id != detailedCategory.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and DetailedCategory.id do not match"));
            }

            await _bll.DetailedCategories.UpdateAsync(_mapper.Map(detailedCategory));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new DetailedCategory
        /// </summary>
        /// <param name="detailedCategory">DetailedCategory object</param>
        /// <returns>created DetailedCategory object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.DetailedCategory))]
        public async Task<ActionResult<V1DTO.DetailedCategory>> PostDetailedCategory(
            V1DTO.DetailedCategory detailedCategory)
        {
            var bllEntity = _mapper.Map(detailedCategory);
            _bll.DetailedCategories.Add(bllEntity);
            await _bll.SaveChangesAsync();
            detailedCategory.Id = bllEntity.Id;

            return CreatedAtAction("GetDetailedCategory",
                new {id = detailedCategory.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                detailedCategory);
        }

        /// <summary>
        /// Delete the DetailedCategory
        /// </summary>
        /// <param name="id">DetailedCategory Id</param>
        /// <returns>deleted DetailedCategory object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.DetailedCategory))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.DetailedCategory>> DeleteDetailedCategory(Guid id)
        {
            var detailedCategory = await _bll.DetailedCategories.FirstOrDefaultAsync(id);
            if (detailedCategory == null)
            {
                return NotFound(new V1DTO.MessageDTO("DetailedCategory not found"));
            }

            await _bll.DetailedCategories.RemoveAsync(detailedCategory);
            await _bll.SaveChangesAsync();

            return Ok(detailedCategory);
        }
    }
}