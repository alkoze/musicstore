using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// ItemStatusesController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class ItemStatusesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly ItemStatusMapper _mapper = new ItemStatusMapper();
        /// <summary>
        /// Constructor
        /// </summary>
        public ItemStatusesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined ItemStatus collection.
        /// </summary>
        /// <returns>List of available ItemStatuss</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.ItemStatus>))]
        public async Task<ActionResult<IEnumerable<V1DTO.ItemStatus>>> GetItemStatuses()
        {
            return Ok((await _bll.ItemStatuses.GetAllAsync()).Select(e => _mapper.Map(e)));
        }

        /// <summary>
        /// Get single ItemStatus
        /// </summary>
        /// <param name="id">ItemStatus Id</param>
        /// <returns>request ItemStatus</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.ItemStatus))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.ItemStatus>> GetItemStatus(Guid id)
        {
            var itemStatus = await _bll.ItemStatuses.FirstOrDefaultAsync(id);

            if (itemStatus == null)
            {
                return NotFound(new V1DTO.MessageDTO("ItemStatus not found"));
            }

            return Ok(_mapper.Map(itemStatus));
        }

        /// <summary>
        /// Update the ItemStatus
        /// </summary>
        /// <param name="id">ItemStatus id</param>
        /// <param name="itemStatus">ItemStatus object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutItemStatus(Guid id, V1DTO.ItemStatus itemStatus)
        {
            if (id != itemStatus.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and ItemStatus.id do not match"));
            }

            await _bll.ItemStatuses.UpdateAsync(_mapper.Map(itemStatus));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new ItemStatus
        /// </summary>
        /// <param name="itemStatus">ItemStatus object</param>
        /// <returns>created ItemStatus object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.ItemStatus))]
        public async Task<ActionResult<V1DTO.ItemStatus>> PostItemStatus(
            V1DTO.ItemStatus itemStatus)
        {
            var bllEntity = _mapper.Map(itemStatus);
            _bll.ItemStatuses.Add(bllEntity);
            await _bll.SaveChangesAsync();
            itemStatus.Id = bllEntity.Id;

            return CreatedAtAction("GetItemStatus",
                new {id = itemStatus.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                itemStatus);
        }

        /// <summary>
        /// Delete the ItemStatus
        /// </summary>
        /// <param name="id">ItemStatus Id</param>
        /// <returns>deleted ItemStatus object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.ItemStatus))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.ItemStatus>> DeleteItemStatus(Guid id)
        {
            var itemStatus = await _bll.ItemStatuses.FirstOrDefaultAsync(id);
            if (itemStatus == null)
            {
                return NotFound(new V1DTO.MessageDTO("ItemStatus not found"));
            }

            await _bll.ItemStatuses.RemoveAsync(itemStatus);
            await _bll.SaveChangesAsync();

            return Ok(itemStatus);
        }
    }
}