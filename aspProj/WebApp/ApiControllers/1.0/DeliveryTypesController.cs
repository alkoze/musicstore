using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// DeliveryTypesController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class DeliveryTypesController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly DeliveryTypeMapper _mapper = new DeliveryTypeMapper();

        /// <summary>
        /// Constructor
        /// </summary>
        public DeliveryTypesController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined DeliveryType collection.
        /// </summary>
        /// <returns>List of available DeliveryTypes</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.DeliveryType>))]
        public async Task<ActionResult<IEnumerable<V1DTO.DeliveryType>>> GetDeliveryTypes()
        {
            return Ok((await _bll.DeliveryTypes.GetAllAsync()).Select(e => _mapper.Map(e)));
        }

        /// <summary>
        /// Get single DeliveryType
        /// </summary>
        /// <param name="id">DeliveryType Id</param>
        /// <returns>request DeliveryType</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.DeliveryType))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.DeliveryType>> GetDeliveryType(Guid id)
        {
            var deliveryType = await _bll.DeliveryTypes.FirstOrDefaultAsync(id);

            if (deliveryType == null)
            {
                return NotFound(new V1DTO.MessageDTO("DeliveryType not found"));
            }

            return Ok(_mapper.Map(deliveryType));
        }

        /// <summary>
        /// Update the DeliveryType
        /// </summary>
        /// <param name="id">DeliveryType id</param>
        /// <param name="deliveryType">DeliveryType object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutDeliveryType(Guid id, V1DTO.DeliveryType deliveryType)
        {
            if (id != deliveryType.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and DeliveryType.id do not match"));
            }

            await _bll.DeliveryTypes.UpdateAsync(_mapper.Map(deliveryType));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new DeliveryType
        /// </summary>
        /// <param name="deliveryType">DeliveryType object</param>
        /// <returns>created DeliveryType object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.DeliveryType))]
        public async Task<ActionResult<V1DTO.DeliveryType>> PostDeliveryType(
            V1DTO.DeliveryType deliveryType)
        {
            var bllEntity = _mapper.Map(deliveryType);
            _bll.DeliveryTypes.Add(bllEntity);
            await _bll.SaveChangesAsync();
            deliveryType.Id = bllEntity.Id;

            return CreatedAtAction("GetDeliveryType",
                new {id = deliveryType.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                deliveryType);
        }

        /// <summary>
        /// Delete the DeliveryType
        /// </summary>
        /// <param name="id">DeliveryType Id</param>
        /// <returns>deleted DeliveryType object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.DeliveryType))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.DeliveryType>> DeleteDeliveryType(Guid id)
        {
            var deliveryType = await _bll.DeliveryTypes.FirstOrDefaultAsync(id);
            if (deliveryType == null)
            {
                return NotFound(new V1DTO.MessageDTO("DeliveryType not found"));
            }

            await _bll.DeliveryTypes.RemoveAsync(deliveryType);
            await _bll.SaveChangesAsync();

            return Ok(deliveryType);
        }
    }
}