using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Contracts.BLL.App;
using DAL.App.EF;
using Domain.App;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PublicApi.DTO.v1.Mappers;
using V1DTO = PublicApi.DTO.v1;


namespace WebApp.ApiControllers._1._0
{
    /// <summary>
    /// PaymentMethodsController api
    /// </summary>
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ProducesResponseType(StatusCodes.Status401Unauthorized)]
    public class PaymentMethodsController : ControllerBase
    {
        private readonly IAppBLL _bll;
        private readonly PaymentMethodMapper _mapper = new PaymentMethodMapper();
        /// <summary>
        /// Constructor
        /// </summary>
        public PaymentMethodsController(IAppBLL bll)
        {
            _bll = bll;
        }

        /// <summary>
        /// Get the predefined PaymentMethod collection.
        /// </summary>
        /// <returns>List of available PaymentMethods</returns>
        [HttpGet]
        [AllowAnonymous]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(IEnumerable<V1DTO.PaymentMethod>))]
        public async Task<ActionResult<IEnumerable<V1DTO.PaymentMethod>>> PaymentMethods()
        {
            return Ok((await _bll.PaymentMethods.GetAllAsync()).Select(e => _mapper.Map(e)));
        }

        /// <summary>
        /// Get single PaymentMethod
        /// </summary>
        /// <param name="id">PaymentMethod Id</param>
        /// <returns>request PaymentMethod</returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        [Produces("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.PaymentMethod))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.PaymentMethod>> GetPaymentMethod(Guid id)
        {
            var paymentMethod = await _bll.PaymentMethods.FirstOrDefaultAsync(id);

            if (paymentMethod == null)
            {
                return NotFound(new V1DTO.MessageDTO("PaymentMethod not found"));
            }

            return Ok(_mapper.Map(paymentMethod));
        }

        /// <summary>
        /// Update the PaymentMethod
        /// </summary>
        /// <param name="id">PaymentMethod id</param>
        /// <param name="paymentMethod">PaymentMethod object</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        [ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(V1DTO.MessageDTO))]
        public async Task<IActionResult> PutPaymentMethod(Guid id, V1DTO.PaymentMethod paymentMethod)
        {
            if (id != paymentMethod.Id)
            {
                return BadRequest(new V1DTO.MessageDTO("id and PaymentMethod.id do not match"));
            }

            await _bll.PaymentMethods.UpdateAsync(_mapper.Map(paymentMethod));
            await _bll.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Create a new PaymentMethod
        /// </summary>
        /// <param name="paymentMethod">PaymentMethod object</param>
        /// <returns>created PaymentMethod object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpPost]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status201Created, Type = typeof(V1DTO.PaymentMethod))]
        public async Task<ActionResult<V1DTO.PaymentMethod>> PostPaymentMethod(
            V1DTO.PaymentMethod paymentMethod)
        {
            var bllEntity = _mapper.Map(paymentMethod);
            _bll.PaymentMethods.Add(bllEntity);
            await _bll.SaveChangesAsync();
            paymentMethod.Id = bllEntity.Id;

            return CreatedAtAction("GetPaymentMethod",
                new {id = paymentMethod.Id, version = HttpContext.GetRequestedApiVersion()?.ToString() ?? "0"},
                paymentMethod);
        }

        /// <summary>
        /// Delete the PaymentMethod
        /// </summary>
        /// <param name="id">PaymentMethod Id</param>
        /// <returns>deleted PaymentMethod object</returns>
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "admin")]
        [HttpDelete("{id}")]
        [Produces("application/json")]
        [Consumes("application/json")]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(V1DTO.PaymentMethod))]
        [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(V1DTO.MessageDTO))]
        public async Task<ActionResult<V1DTO.PaymentMethod>> DeletePaymentMethod(Guid id)
        {
            var paymentMethod = await _bll.PaymentMethods.FirstOrDefaultAsync(id);
            if (paymentMethod == null)
            {
                return NotFound(new V1DTO.MessageDTO("PaymentMethod not found"));
            }

            await _bll.PaymentMethods.RemoveAsync(paymentMethod);
            await _bll.SaveChangesAsync();

            return Ok(paymentMethod);
        }
    }
}