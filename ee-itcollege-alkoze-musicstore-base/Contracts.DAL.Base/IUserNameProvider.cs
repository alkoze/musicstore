namespace ee.itcollege.alkoze.Contracts.DAL.Base
{
    public interface IUserNameProvider
    {
        string CurrentUserName { get; }
    }
}