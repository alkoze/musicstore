namespace ee.itcollege.alkoze.musicstoreContracts.BLL.Base.Mappers
{
    public interface IBaseMapper<TLeftObject, TRightObject> : ee.itcollege.alkoze.Contracts.DAL.Base.Mappers.IBaseMapper<TLeftObject, TRightObject>
        where TLeftObject: class?, new()
        where TRightObject: class?, new()
    {
        
    }
}