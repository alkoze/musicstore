using System;
using ee.itcollege.alkoze.Contracts.DAL.Base.Repositories;
using ee.itcollege.alkoze.musicstore.Contracts.Domain;

namespace ee.itcollege.alkoze.musicstoreContracts.BLL.Base.Services
{
    public interface IBaseEntityService<TEntity> : IBaseEntityService<Guid, TEntity>
        where TEntity : class, IDomainEntityId<Guid>, new()
    {
    }

    public interface IBaseEntityService<in TKey, TEntity> : IBaseService, IBaseRepository<TKey, TEntity>
        where TKey : IEquatable<TKey>
        where TEntity : class, IDomainEntityId<TKey>, new()
    {
        
    }

}
        
 