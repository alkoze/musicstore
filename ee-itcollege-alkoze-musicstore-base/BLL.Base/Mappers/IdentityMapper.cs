namespace ee.itcollege.musicstore.BLL.Base.Mappers
{
    public class IdentityMapper<TLeftObject, TRightObject> : ee.itcollege.alkoze.musicstore.DAL.Base.Mappers.IdentityMapper<TLeftObject, TRightObject>
        where TRightObject : class?, new() 
        where TLeftObject : class?, new()
    {
    }
}