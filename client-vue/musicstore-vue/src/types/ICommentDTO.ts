export interface ICommentDTO {
  id: string;
  commentText: string;
  appUserId: string;
  ratingId: string;
  creator: string;
}