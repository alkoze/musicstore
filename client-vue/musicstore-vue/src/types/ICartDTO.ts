import { IItem } from '@/domain/IItem';

export interface ICartDTO{
  id: string,
  items: IItem[],
  appUserId: string,
}