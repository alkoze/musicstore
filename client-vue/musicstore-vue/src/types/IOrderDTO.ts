import { IItem } from '@/domain/IItem';

export interface IOrderDTO{
  id: string;
  deliveryTypeId: string;
  appUserId: string;
  orderPrice: number;
  paymentInfo: string;
  deliveryAddress: string;
  postalIndex: string;
  paymentMethodId: string;
}