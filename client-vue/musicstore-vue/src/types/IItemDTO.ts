export interface IItemDTO{
id:	string;
itemName:	string;
yearMade:	number;
itemDescription: string;
price: number;
locationId:	string;
itemQualityId:	string;
itemStatusId:	string;
detailedCategoryId:	string;
mainCategoryId:	string;
appUserId: string;
}