export interface IItemInCartDTO {
  itemid: string;
  cartId: string;
  id: string;
}