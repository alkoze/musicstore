import { IDetailedCategory } from './IDetailedCategory';

export interface IMainCategory{
  id: string;
  mainCategoryName: string;
  detailedCategories: IDetailedCategory[]
}