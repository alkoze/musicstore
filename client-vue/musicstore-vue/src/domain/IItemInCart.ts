export interface IItemInCart {
  id: string,
  itemId: string,
  cartId: string
}