import { IItem } from './IItem';

export interface IOrder {
  id: string;
  deliveryTypeId: string;
  deliveryTypeName: string;
  items: IItem[];
  appUserId: string;
  orderPrice: number;
  paymentInfo: string;
  deliveryAddress: string;
  paymentMethodName: string;
  paymentMethodId: string;
  postalIndex: string;
}