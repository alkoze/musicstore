export interface IRating {
  id: string;
  ratingValue: string;
}