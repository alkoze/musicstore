export interface ISpecificCulture {
  code: {
    name: string;
  }
}