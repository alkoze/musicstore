import { IItem } from './IItem';

export interface ICart{
  id: string;
  items: IItem[];
  appUserId: string;
  itemCount: number;
}