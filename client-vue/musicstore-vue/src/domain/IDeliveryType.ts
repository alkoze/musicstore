export interface IDeliveryType {
  id: string;
  deliveryTypeName: string;
  deliveryTypePrice: number;
}