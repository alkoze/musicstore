import { IPicture } from './IPicture';


export interface IItem{
  id: string;
  itemName: string;
  yearMade: number;
  itemDescription: string;
  price: number;
  Location: string;
  locationId: string;
  ItemQuality: string;
  itemQualityId: string;
  itemStatus: string;
  itemstatusId: string;
  DetailedCategory: string;
  detailedCategoryId: string;
  MainCategory: string;
  mainCategoryId: string;
  appUserId: string;
  orderId: string | null;
  displayPictureUrl: string;
  sellerName: string;
  pictures: IPicture[];
}