export interface IPicture{
  id: string;
  itemId: string;
  pictureUrl: string;
}