export interface IComment {
  id: string;
  commentText: string;
  appUserId: string;
  rating: string;
  creator: string;
}