import { IItem } from './IItem';

export interface IDetailedCategory{
  id: string;
  detailedCategoryName: string;
  mainCategoryId: string;
  mainCategory: string;
  items: IItem[]
}