import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'
import AccountLogin from '../views/Account/Login.vue'
import AccountRegister from '../views/Account/Register.vue'
import ItemsIndex from '../views/Items/Index.vue'
import ItemDetails from '../views/Items/Details.vue'
import ItemCreate from '../views/Items/Create.vue'
import CartDetails from '../views/Carts/Details.vue'
import ItemEdit from '../views/Items/Edit.vue'
import MyItems from '../views/Items/MyItems.vue'
import MyOrders from '../views/Order/MyOrders.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
    { path: '/', name: 'Home', component: Home },
    { path: '/account/login', name: 'AccountLogin', component: AccountLogin },
    { path: '/account/register', name: 'AccountRegister', component: AccountRegister },
    { path: '/items', name: 'Items', component: ItemsIndex },
    { path: '/items/details/:id?', name: 'ItemDetails', component: ItemDetails, props: true },
    { path: '/items/create', name: 'ItemCreate', component: ItemCreate },
    { path: '/carts/details', name: 'CartDetails', component: CartDetails },
    { path: '/items/edit/:id?', name: 'ItemEdit', component: ItemEdit, props: true },
    { path: '/items/myitems', name: 'MyItems', component: MyItems },
    { path: '/order/myorders', name: 'MyOrders', component: MyOrders }
]

const router = new VueRouter({
    routes
})

export default router
