import Vue from 'vue'
import Vuex, { Store } from 'vuex'
import { ILoginDTO } from '@/types/ILoginDTO';
import { AccountApi } from '@/services/AccountApi';
import { IRegisterDTO } from '@/types/IRegisterDTO';
import { IItem } from '@/domain/IItem'
import { ItemsApi } from '@/services/ItemsApi';
import { IItemDTO } from '@/types/IItemDTO';
import { ILocation } from '@/domain/ILocation';
import { LocationsApi } from '@/services/LocationsApi';
import { DetailedCategoriesApi } from '@/services/DetailedCategoriesApi';
import { IDetailedCategory } from '@/domain/IDetailedCategory';
import { IMainCategory } from '@/domain/IMainCategory';
import { MainCategoriesApi } from '@/services/MainCategoriesApi';
import { IItemQuality } from '@/domain/IItemQuality';
import { ItemQualitiesApi } from '@/services/ItemQualitiesApi';
import { IPictureDTO } from '@/types/IPictureDTO';
import { PicturesApi } from '@/services/PicturesApi';
import { ICartDTO } from '@/types/ICartDTO';
import { CartsApi } from '@/services/CartsApi';
import { ICart } from '@/domain/ICart';
import { IItemInCartDTO } from '@/types/IItemInCartDTO';
import { ItemInCartsApi } from '@/services/ItemInCartsApi';
import { IOrder } from '@/domain/IOrder';
import { IOrderDTO } from '@/types/IOrderDTO';
import { OrdersApi } from '@/services/OrdersApi';
import { IPaymentMethod } from '@/domain/IPaymentMethod';
import { IDeliveryType } from '@/domain/IDeliveryType';
import { DeliveryTypesApi } from '@/services/DeliveryTypesApi';
import { PaymentMethodsApi } from '@/services/PaymentMethodsApi';
import { IItemInCart } from '@/domain/IItemInCart';
import { IComment } from '@/domain/IComment';
import { CommentsApi } from '@/services/CommentsApi';
import { ICommentDTO } from '@/types/ICommentDTO';
import { IRating } from '@/domain/IRating';
import { RatingsApi } from '@/services/RatingsApi';
import { ILinkDTO } from '@/types/ILinkDTO';
import { IPicture } from '@/domain/IPicture';
import { ICulture } from '@/domain/ICulture';
import { CultureApi } from '@/services/CultureApi';
import { ISpecificCulture } from '@/domain/ISpecificCulture';

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        jwt: null as string | null,
        register: {} as IRegisterDTO,
        items: [] as IItem[],
        item: {} as IItem,
        newItem: {} as IItemDTO,
        locations: [] as ILocation[],
        detailedCategories: [] as IDetailedCategory[],
        mainCategories: [] as IMainCategory[],
        itemQualities: [] as IItemQuality[],
        newPicture: {} as IPictureDTO,
        newCart: {} as ICartDTO,
        carts: [] as ICart[],
        itemInCarts: [] as IItemInCart[],
        newItemInCart: {} as IItemInCartDTO,
        orders: [] as IOrder[],
        newOrder: {} as IOrderDTO,
        paymentMethods: [] as IPaymentMethod[],
        deliveryTypes: [] as IDeliveryType[],
        comments: [] as IComment[],
        ratings: [] as IRating[],
        pictures: [] as IPicture[],
        cultures: [] as ICulture[],
        specificCultures: [] as ICulture[],
        vuecultures: [] as ICulture[],
        vuespecificCultures: [] as ICulture[],
        culture: { code: 'en-GB', name: 'English GB' } as ICulture
    },
    mutations: {
        setJwt(state, jwt: string | null) {
            state.jwt = jwt;
        },
        setPictures(state, pictures: IPicture[]) {
            state.pictures = pictures
        },
        setRatings(state, ratings: IRating[]) {
            state.ratings = ratings
        },
        setPaymentMethods(state, paymentMethods: IPaymentMethod[]) {
            state.paymentMethods = paymentMethods
        },
        setComments(state, comments: IComment[]) {
            state.comments = comments
        },
        setDeliveryTypes(state, deliveryTypes: IDeliveryType[]) {
            state.deliveryTypes = deliveryTypes
        },
        setRegister(state, register: IRegisterDTO) {
            state.register = register
        },
        setOrders(state, orders: IOrder[]) {
            state.orders = orders;
        },
        setNewOrder(state, newOrder: IOrderDTO) {
            state.newOrder = newOrder;
        },
        setItems(state, items: IItem[]) {
            state.items = items;
        },
        setItem(state, item: IItem) {
            state.item = item;
        },
        setNewItem(state, newItem: IItemDTO) {
            state.newItem = newItem
        },
        setNewPicture(state, newPicture: IPictureDTO) {
            state.newPicture = newPicture
        },
        setNewItemInCart(state, newItemInCart: IItemInCartDTO) {
            state.newItemInCart = newItemInCart
        },
        setItemInCarts(state, itemInCarts: IItemInCart[]) {
            state.itemInCarts = itemInCarts
        },
        setNewCart(state, newCart: ICartDTO) {
            state.newCart = newCart
        },
        setCarts(state, carts: ICart[]) {
            state.carts = carts
        },
        setLocations(state, locations: ILocation[]) {
            state.locations = locations;
        },
        setDetailedCategories(state, detailedCategories: IDetailedCategory[]) {
            state.detailedCategories = detailedCategories;
        },
        setMainCategories(state, mainCategories: IMainCategory[]) {
            state.mainCategories = mainCategories;
        },
        setItemQualities(state, itemQualities: IItemQuality[]) {
            state.itemQualities = itemQualities;
        },
        setCultures(state, cultures: ICulture[]) {
            state.cultures = cultures;
        },
        setSpecificCultures(state, specificCultures: ICulture[]) {
            state.specificCultures = specificCultures;
        },
        setCulture(state, culture: ICulture) {
            state.culture = culture;
        }
    },
    getters: {
        isAuthenticated(context): boolean {
            return context.jwt !== null;
        }
    },
    actions: {
        clearJwt(context): void {
            context.commit('setJwt', null);
        },
        async authenticateUser(context, loginDTO: ILoginDTO): Promise<boolean> {
            const jwt = await AccountApi.getJwt(loginDTO);
            context.commit('setJwt', jwt);
            return jwt !== null;
        },
        async register(context, registerDTO: IRegisterDTO): Promise<boolean> {
            const acc = await AccountApi.register(registerDTO);
            return acc;
        },
        async getPictures(context): Promise<void> {
            const pictures = await PicturesApi.getAll();
            context.commit('setPictures', pictures);
        },
        async getRatings(context): Promise<void> {
            const ratings = await RatingsApi.getAll();
            context.commit('setRatings', ratings);
        },
        async getComments(context): Promise<void> {
            const comments = await CommentsApi.getAll();
            context.commit('setComments', comments);
        },
        async createComment(context, commentDTO: ICommentDTO): Promise<boolean> {
            const newComment = await CommentsApi.create(commentDTO);
            context.dispatch('getComments')
            return newComment;
        },
        async getLocations(context): Promise<void> {
            const locations = await LocationsApi.getAll();
            context.commit('setLocations', locations);
        },
        async getDeliveryTypes(context): Promise<void> {
            const deliveryTypes = await DeliveryTypesApi.getAll();
            context.commit('setDeliveryTypes', deliveryTypes);
        },
        async getPaymentMethods(context): Promise<void> {
            const paymentMethods = await PaymentMethodsApi.getAll();
            context.commit('setPaymentMethods', paymentMethods);
        },
        async getMainCategories(context): Promise<void> {
            const mainCategories = await MainCategoriesApi.getAll();
            context.commit('setMainCategories', mainCategories);
        },
        async getDetailedCategories(context): Promise<void> {
            const detailedCategories = await DetailedCategoriesApi.getAll();
            context.commit('setDetailedCategories', detailedCategories);
        },
        async getItemQualities(context): Promise<void> {
            const itemQualities = await ItemQualitiesApi.getAll();
            context.commit('setItemQualities', itemQualities);
        },
        async createPicture(context, pictureDTO: IPictureDTO): Promise<boolean> {
            const newPicture = await PicturesApi.create(pictureDTO);
            return newPicture;
        },
        async getOrders(context): Promise<void> {
            const orders = await OrdersApi.getAll();
            context.commit('setOrders', orders);
        },
        async createOrder(context, orderDTO: IOrderDTO): Promise<boolean> {
            const newOrder = await OrdersApi.create(orderDTO);
            return newOrder;
        },
        async getItemInCarts(context): Promise<void> {
            const itemInCarts = await ItemInCartsApi.getAll();
            context.commit('setItemInCarts', itemInCarts);
        },
        async createItemInCart(context, itemInCartDTO: IItemInCartDTO): Promise<boolean> {
            const newItemInCart = await ItemInCartsApi.create(itemInCartDTO);
            context.dispatch('getCarts')
            return newItemInCart;
        },
        async createCart(context, cartDTO: ICartDTO): Promise<boolean> {
            const newCart = await CartsApi.create(cartDTO);
            return newCart;
        },
        async getCarts(context): Promise<void> {
            const carts = await CartsApi.getAll();
            context.commit('setCarts', carts);
        },
        async updateCart(context, cartDTO: ICartDTO): Promise<boolean> {
            const cart = await CartsApi.update(cartDTO);
            return cart;
        },
        async getItems(context): Promise<void> {
            const items = await ItemsApi.getAll();
            context.commit('setItems', items);
        },
        async getOneItem(context, id: string): Promise<void> {
            const item = await ItemsApi.getById(id);
            context.commit('setItem', item)
        },
        async createItem(context, itemDTO: IItemDTO): Promise<boolean> {
            const newItem = await ItemsApi.create(itemDTO);
            await context.dispatch('getItems');
            return newItem;
        },
        async deletePicture(context, id: string): Promise<void> {
            console.log('deleteItem', context.getters.isAuthenticated);
            await PicturesApi.delete(id);
            await context.dispatch('getItems');
        },
        async deleteItem(context, id: string): Promise<void> {
            console.log('deleteItem', context.getters.isAuthenticated);
            if (context.getters.isAuthenticated && context.state.jwt) {
                await ItemsApi.delete(id, context.state.jwt);
                await context.dispatch('getItems');
            }
        },
        async deleteItemInCart(context, id: string): Promise<void> {
            console.log('deleteCart', context.getters.isAuthenticated);
            if (context.getters.isAuthenticated && context.state.jwt) {
                await ItemInCartsApi.delete(id, context.state.jwt);
                await context.dispatch('getCarts');
            }
        },
        async updateItem(context, item: IItem): Promise<boolean> {
            const updated = await ItemsApi.update(item);
            await ItemsApi.update(item)
            await context.dispatch('getItems')
            return updated
        },
        async getCultures(context): Promise<void> {
            const Cultures = await CultureApi.getAll();
            context.commit('setCultures', Cultures);
        },
        async getSpecificCultures(context): Promise<void> {
            let Cultures = await CultureApi.getAllForItems();
            const VueTrans = await CultureApi.getAllForVue();
            Cultures = Cultures.concat(VueTrans);
            context.commit('setSpecificCultures', Cultures);
        },
        async setCulture(context, Culture: ICulture): Promise<void> {
            context.commit('setCulture', Culture);
            await context.dispatch('getSpecificCultures')
        }
    },
    modules: {
    }
})
