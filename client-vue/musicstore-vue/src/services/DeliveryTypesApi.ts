import Axios from 'axios';
import { IPaymentMethod } from '@/domain/IPaymentMethod';
import { IDeliveryType } from '@/domain/IDeliveryType';

export abstract class DeliveryTypesApi {
  private static axios = Axios.create(
      {
          baseURL: "https://musicstore2020.azurewebsites.net/api/v1/DeliveryTypes/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<IDeliveryType[]> {
      const url = "";
      try {
          const response = await this.axios.get<IDeliveryType[]>(url);
          console.log('getAll response', response);
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return [];
      }
  }
}
