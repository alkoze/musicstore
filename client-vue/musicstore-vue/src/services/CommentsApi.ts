import { ICommentDTO } from '@/types/ICommentDTO';
import Axios from 'axios';
import { IComment } from '@/domain/IComment';

export abstract class CommentsApi {
    private static axios = Axios.create(
        {
            baseURL: "https://musicstore2020.azurewebsites.net/api/v1/Comments/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async create(commentDTO: ICommentDTO): Promise<boolean> {
        const url = ""
        try {
            const response = await this.axios.post<ICommentDTO>(url, commentDTO);
            console.log('cart response', response);
            if (response.status === 201) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async getAll(): Promise<IComment[]> {
        const url = "";
        try {
            const response = await this.axios.get<IComment[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }
}
