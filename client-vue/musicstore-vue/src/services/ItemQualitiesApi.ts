import Axios from 'axios';
import { IItemQuality } from '@/domain/IItemQuality';

export abstract class ItemQualitiesApi {
  private static axios = Axios.create(
      {
          baseURL: "https://musicstore2020.azurewebsites.net/api/v1/ItemQualities/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<IItemQuality[]> {
      const url = "";
      try {
          const response = await this.axios.get<IItemQuality[]>(url);
          console.log('getAll response', response);
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return [];
      }
  }
}
