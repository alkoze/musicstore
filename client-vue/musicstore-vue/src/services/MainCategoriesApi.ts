import Axios from 'axios';
import { IMainCategory } from '@/domain/IMainCategory';

export abstract class MainCategoriesApi {
  private static axios = Axios.create(
      {
          baseURL: "https://musicstore2020.azurewebsites.net/api/v1/MainCategories/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<IMainCategory[]> {
      const url = "";
      try {
          const response = await this.axios.get<IMainCategory[]>(url);
          console.log('getAll response', response);
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return [];
      }
  }
}
