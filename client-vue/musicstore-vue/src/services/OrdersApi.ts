import Axios from 'axios';
import { IPaymentMethod } from '@/domain/IPaymentMethod';
import { IOrder } from '@/domain/IOrder';
import { IOrderDTO } from '@/types/IOrderDTO';

export abstract class OrdersApi {
  private static axios = Axios.create(
      {
          baseURL: "https://musicstore2020.azurewebsites.net/api/v1/Orders/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<IOrder[]> {
      const url = "";
      try {
          const response = await this.axios.get<IOrder[]>(url);
          console.log('getAll response', response);
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return [];
      }
  }

  static async create(orderDTO: IOrderDTO): Promise<boolean> {
      const url = ""
      try {
          const response = await this.axios.post<IOrderDTO>(url, orderDTO);
          console.log('cart response', response);
          if (response.status === 201) {
              return true
          }
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return false
      }
      return false
  }
}
