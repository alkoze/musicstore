import Axios from 'axios';
import { IPaymentMethod } from '@/domain/IPaymentMethod';
import { IDeliveryType } from '@/domain/IDeliveryType';
import { IRating } from '@/domain/IRating';

export abstract class RatingsApi {
  private static axios = Axios.create(
      {
          baseURL: "https://musicstore2020.azurewebsites.net/api/v1/Ratings/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<IRating[]> {
      const url = "";
      try {
          const response = await this.axios.get<IRating[]>(url);
          console.log('getAll response', response);
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return [];
      }
  }
}
