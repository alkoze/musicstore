import Axios from 'axios';
import { IItemInCartDTO } from '@/types/IItemInCartDTO';
import { IItemInCart } from '@/domain/IItemInCart';

export abstract class ItemInCartsApi {
    private static axios = Axios.create(
        {
            baseURL: "https://musicstore2020.azurewebsites.net/api/v1/ItemInCarts/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IItemInCart[]> {
        const url = "";
        try {
            const response = await this.axios.get<IItemInCart[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async create(itemInCartDTO: IItemInCartDTO): Promise<boolean> {
        const url = ""
        try {
            const response = await this.axios.post<IItemInCartDTO>(url, itemInCartDTO);
            console.log('ItemInCart response', response);
            if (response.status === 201) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async delete(id: string, jwt: string): Promise<void> {
        const url = "" + id;
        try {
            const response = await this.axios.delete<IItemInCart>(url, { headers: { Authorization: 'Bearer ' + jwt } });
            console.log('delete response', response);
            if (response.status === 200) {
                return;
            }
            return;
        } catch (error) {
            console.log('error: ', (error as Error).message);
        }
    }
}
