import Axios from 'axios';
import { IPaymentMethod } from '@/domain/IPaymentMethod';

export abstract class PaymentMethodsApi {
  private static axios = Axios.create(
      {
          baseURL: "https://musicstore2020.azurewebsites.net/api/v1/PaymentMethods/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<IPaymentMethod[]> {
      const url = "";
      try {
          const response = await this.axios.get<IPaymentMethod[]>(url);
          console.log('getAll response', response);
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return [];
      }
  }
}
