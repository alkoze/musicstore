import { ICartDTO } from '@/types/ICartDTO';
import Axios from 'axios';
import { ICart } from '@/domain/ICart';

export abstract class CartsApi {
    private static axios = Axios.create(
        {
            baseURL: "https://musicstore2020.azurewebsites.net/api/v1/Carts/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async create(cartDTO: ICartDTO): Promise<boolean> {
        const url = ""
        try {
            const response = await this.axios.post<ICartDTO>(url, cartDTO);
            console.log('cart response', response);
            if (response.status === 201) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async getAll(): Promise<ICart[]> {
        const url = "";
        try {
            const response = await this.axios.get<ICart[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async update(cartDTO: ICartDTO): Promise<boolean> {
        const url = "" + cartDTO.id;
        try {
            const response = await this.axios.put<ICartDTO>(url, cartDTO);
            console.log('cart response', response);
            if (response.status === 204) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }
}
