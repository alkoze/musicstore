import Axios from 'axios';
import { ICulture } from '@/domain/ICulture';
import store from '@/store';
import { ISpecificCulture } from '@/domain/ISpecificCulture';

export abstract class CultureApi {
  private static axios = Axios.create(
      {
          baseURL: "https://musicstore2020.azurewebsites.net/api/v1.0/Cultures/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<ICulture[]> {
      const url = "";
      try {
          const response = await this.axios.get<ICulture[]>(url)
          console.log('getAll response', response)
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message)
          return [];
      }
  }

  static async getAllForItems(): Promise<ICulture[]> {
      const url = "items/?language=" + store.state.culture.code;
      try {
          const response = await this.axios.get<ICulture[]>(url)
          console.log('getAll response', response)
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message)
          return [];
      }
  }

  static async getAllForVue(): Promise<ICulture[]> {
      const url = "Vue/?language=" + store.state.culture.code;
      try {
          const response = await this.axios.get<ICulture[]>(url)
          console.log('getAll response', response)
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message)
          return [];
      }
  }
}
