import { IPictureDTO } from '@/types/IPictureDTO';
import Axios from 'axios';
import { IPicture } from '@/domain/IPicture';

export abstract class PicturesApi {
    private static axios = Axios.create(
        {
            baseURL: "https://musicstore2020.azurewebsites.net/api/v1/Pictures/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async create(cartDTO: IPictureDTO): Promise<boolean> {
        const url = ""
        try {
            const response = await this.axios.post<IPictureDTO>(url, cartDTO);
            console.log('picture response', response);
            if (response.status === 201) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async getAll(): Promise<IPicture[]> {
        const url = "";
        try {
            const response = await this.axios.get<IPicture[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async delete(id: string): Promise<void> {
        const url = "" + id;
        try {
            const response = await this.axios.delete<IPicture>(url);
            console.log('delete response', response);
            if (response.status === 200) {
                return;
            }
            return;
        } catch (error) {
            console.log('error: ', (error as Error).message);
        }
    }
}
