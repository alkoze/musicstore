import Axios from 'axios';
import { IDetailedCategory } from '@/domain/IDetailedCategory';

export abstract class DetailedCategoriesApi {
  private static axios = Axios.create(
      {
          baseURL: "https://musicstore2020.azurewebsites.net/api/v1/DetailedCategories/",
          headers: {
              common: {
                  'Content-Type': 'application/json'
              }
          }
      }
  )

  static async getAll(): Promise<IDetailedCategory[]> {
      const url = "";
      try {
          const response = await this.axios.get<IDetailedCategory[]>(url);
          console.log('getAll response', response);
          if (response.status === 200) {
              return response.data;
          }
          return [];
      } catch (error) {
          console.log('error: ', (error as Error).message);
          return [];
      }
  }
}
