import Axios from 'axios'
import { IItem } from '@/domain/IItem'
import { IItemDTO } from '@/types/IItemDTO';

export abstract class ItemsApi {
    private static axios = Axios.create(
        {
            baseURL: "https://musicstore2020.azurewebsites.net/api/v1/Items/",
            headers: {
                common: {
                    'Content-Type': 'application/json'
                }
            }
        }
    )

    static async getAll(): Promise<IItem[]> {
        const url = "";
        try {
            const response = await this.axios.get<IItem[]>(url);
            console.log('getAll response', response);
            if (response.status === 200) {
                return response.data;
            }
            return [];
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return [];
        }
    }

    static async getById(id: string): Promise<IItem> {
        const url = "" + id;
        try {
            const response = await this.axios.get<IItem>(url);
            console.log('getById response', response);
            if (response.status === 200) {
                return response.data;
            }
            return response.data;
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return error;
        }
    }

    static async create(itemDTO: IItemDTO): Promise<boolean> {
        const url = ""
        try {
            const response = await this.axios.post<IItemDTO>(url, itemDTO);
            console.log('item response', response);
            if (response.status === 201) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async update(item: IItem): Promise<boolean> {
        const url = "" + item.id
        try {
            const response = await this.axios.put<IItem>(url, item);
            console.log('item response', response.status);
            if (response.status === 204) {
                return true
            }
        } catch (error) {
            console.log('error: ', (error as Error).message);
            return false
        }
        return false
    }

    static async delete(id: string, jwt: string): Promise<void> {
        const url = "" + id;
        try {
            const response = await this.axios.delete<IItem>(url, { headers: { Authorization: 'Bearer ' + jwt } });
            console.log('delete response', response);
            if (response.status === 200) {
                return;
            }
            return;
        } catch (error) {
            console.log('error: ', (error as Error).message);
        }
    }
}
